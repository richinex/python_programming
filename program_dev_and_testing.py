# -*- coding: utf-8 -*-
"""
Created on Thu Jan 23 00:16:03 2020

@author: Richard
"""
# Plan ahead. Think about what you want to do. Think about your program.
# Have a design
# Keep on testing
# develop iteratively (pyramid style)

# Savings program Development
# Get information from the user. Always test regularly
balance = float(input("Enter how much you want to save: "))
payment = float(input("Enter how much you will save each period: "))
if (payment==0):
    payment = float(input("You cant get something for nothing! Enter some non-zero value:"))
print("balance is", balance, "and payment is", payment)

# Division to calculate the number of payments
num_remaining_payments = balance/payment

# present the results to the user
print("You must make", num_remaining_payments, "more payments.")

# Take care of zero value entered
balance = float(input("Enter how much you want to save: "))
payment = float(input("Enter how much you will save each period: "))
if (payment==0):
    payment = float(input("You cant get something for nothing! Enter some non-zero value:"))
# Division to calculate the number of payments
num_remaining_payments = balance/payment

# present the results to the user
print("You must make", num_remaining_payments, "more payments.")

# UML diagrams, flow charts

# Take care of negative value entered
balance = float(input("Enter how much you want to save: "))
if (balance<0):
    print("Looks like you already have saved enough!")
    balance = 0
payment = float(input("Enter how much you will save each period: "))
if (payment<=0):
    payment = float(input("Savings must be positive! Please enter a positive value:"))
# Division to calculate the number of payments
num_remaining_payments = balance/payment

# present the results to the user
print("You must make", num_remaining_payments, "more payments.")

# Take care of where payment value entered after 0 balance
balance = float(input("Enter how much you want to save: "))
if (balance<0):
    print("Looks like you already have saved enough!")
    balance = 0
    payment = 1
else:
    payment = float(input("Enter how much you will save each period: "))
    if (payment<=0):
        payment = float(input("Savings must be positive! Please enter a positive value:"))
# Division to calculate the number of payments
num_remaining_payments = balance/payment
# present the results to the user
print("You must make", num_remaining_payments, "more payments.")

#Improvements to the savings program. Iterative development
#Interact more with the user
#Round to the highest integer
#Many more: e.g print a message about what the program is for
print("I'll help you determine how long you will need to save.")
name = input("What's your name?")
item = input("What are you saving up for?")
balance = float(input("OK, ", name, ".Please enter the cost of the ", item, ": "))
if (balance<0):
    print("Looks like you already have saved enough!")
    balance = 0
    payment = 1
else:
    payment = float(input("Enter how much you will save each period: "))
    if (payment<=0):
        payment = float(input("Savings must be positive! Please enter a positive value:"))
# Division to calculate the number of payments
num_remaining_payments = balance/payment
# present the results to the user
print(name, ", You must make", num_remaining_payments, "more payments, and then you'll have your ", item, "!")

# A case where a user enters a negative payment a second time

print("I'll help you determine how long you will need to save.")
name = input("What's your name?")
item = input("What are you saving up for?")
balance = float(input("OK, ", name, ".Please enter the cost of the ", item, ": "))
if (balance<0):
    print("Looks like you already have saved enough!")
    balance = 0
    payment = 1
else:
    payment = float(input("Enter how much you will save each period: "))
    if (payment<=0):
        payment = float(input("Savings must be positive! Please enter a positive value:"))
        if (payment<=0):
            print(name+", you still didn't enter a positive number! I am going to just assume you save 1 per period.")
            payment = 1
# Division to calculate the number of payments
num_remaining_payments = balance/payment
# present the results to the user
print(name, ", You must make", num_remaining_payments, "more payments, and then you'll have your ", item, "!")

# Convert the number of payments to an interget, and add 1 if we had to drop a fraction
print("I'll help you determine how long you will need to save.")
name = input("What's your name?")
item = input("What are you saving up for?")
balance = float(input("OK, ", name, ".Please enter the cost of the ", item, ": "))
if (balance<0):
    print("Looks like you already have saved enough!")
    balance = 0
    payment = 1
else:
    payment = float(input("Enter how much you will save each period: "))
    if (payment<=0):
        payment = float(input("Savings must be positive! Please enter a positive value:"))
        if (payment<=0):
            print(name+", you still didn't enter a positive number! I am going to just assume you save 1 per period.")
            payment = 1
# Division to calculate the number of payments
num_remaining_payments = int(balance/payment)
if (num_remaining_payments < balance/payment):
    num_remaining_payments = num_remaining_payments + 1
# present the results to the user
print(name, ", You must make", num_remaining_payments, "more payments, and then you'll have your ", item, "!")

# Plan ahead before actually writing code
# keep on testing
# Develop iteratively (pyramid style)