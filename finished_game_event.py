# -*- coding: utf-8 -*-
"""
Created on Sun Jan 26 14:03:16 2020

@author: Richard Chukwu
"""
######## FINISHED GAME WITH GRAPHICS #############
# We dont need the get move function anymore since we will receive input from the mouse
from random import choice
import pyglet

window = pyglet.window.Window(width=375, height=600, caption="Game Window")

Im1 = pyglet.image.load('anouar.jpg')
Im2 = pyglet.image.load('mike.jpg')
Im3 = pyglet.image.load('sid.jpg')
Im4 = pyglet.image.load('allen.jpg')
Im5 = pyglet.image.load('young.jpg')


def InitializeGrid(board):
    # Initialize Grid by random value
    for i in range(8):
        for j in range(8):
            board[i][j] = choice(['A', 'B', 'C', 'D', 'E'])
    print("Initializing")

def Initialize(board):
    # Initialize game
    # Initialize grid
    InitializeGrid(board)
    #Initialize score
    global score # using global for our immutable variable
    score = 0
    # Initialize turn number
    global turn
    turn = 1
    # Set up graphical info

def ContinueGame(current_score, goal_score = 100):
    # Return false if game should end, true if game is not over
    if (current_score >= goal_score):
        return False
    else:
        return True

def SwapPieces(board, move):
    # Swap pieces on board according to move
    # get original position

    # Swap objects into position
    # here we use the indices passed in as the move parameter inrder to reference the rows and columns. move[2]move[3] are the new row n column
    temp = board[move[0]][move[1]] # copy one into a temporary position
    board[move[0]][move[1]] = board[move[2]][move[3]] #move the other object into the original position
    board[move[2]][move[3]] = temp
    print("Swapping Pieces")
    
    # Get adjacent position
    # Swap objects in two positions

def RemovePieces(board):
    # remove 3-in-a-row and 3-in-a-coulumn pieces
    # Create board to remove-or-not
    remove = [[0, 0, 0, 0, 0, 0, 0, 0],
              [0, 0, 0, 0, 0, 0, 0, 0],
              [0, 0, 0, 0, 0, 0, 0, 0],
              [0, 0, 0, 0, 0, 0, 0, 0],
              [0, 0, 0, 0, 0, 0, 0, 0],
              [0, 0, 0, 0, 0, 0, 0, 0],
              [0, 0, 0, 0, 0, 0, 0, 0],
              [0, 0, 0, 0, 0, 0, 0, 0]]
    # Go through rows
    for i in range(8):
        for j in range(6) :# the first 6 columns
            if (board[i][j] == board[i][j+1]) and (board[i][j] == board[i][j+2]):
                # three in a row are the same
                remove[i][j] = 1;
                remove[i][j+1] = 1;
                remove[i][j+2] =1;
    # go through columns
    for j in range(8):
        for i in range(6):
            if (board[i][j] == board[i+1][j]) and (board[i][j] == board[i+2][j]):
                # three in a row are the same
                remove[i][j] = 1;
                remove[i+1][j] = 1;
                remove[i+2][j] = 1;
    # Eliminate those marked
    global score
    removed_any = False
    for i in range (8):
        for j in range(8):
            if remove[i][j] == 1:
                board[i][j] = 0
                score += 1
                removed_any = True
    return removed_any


######### DROPPING PIECES #########
## Go through column from bottom to top
## Make list of non-zero elements
## copy those elements in from bottom to top
## Put 0s at top
def DropPieces(board):
    # Drop pieces to fill in blanks
    for j in range(8):
        # make list of pieces in the column
        listofpieces = []
        for i in range(8):
            if board[i][j] != 0:
                listofpieces.append(board[i][j])
    # Copy that list into column
    for i in range(len(listofpieces)):
        board[i][j] = listofpieces[i]
    # fill in remainder of column with 0s
    for i in range(len(listofpieces), 8):
        board[i][j] = 0
    #Drop pieces to fill in blanks
    

############ FILLING BLANKS ###############
# Loop through all elements
# If zero, generate a random new piece for that position    
def FillBlanks(board):
    # Fill blanks with random pieces
    for i in range(8):
        for j in range(8):
            if (board[i][j] == 0):
                board[i][j] = choice(['A', 'B', 'C','D', 'E'])
    



def Update(board, move):
    # Update the board according to move
    SwapPieces(board, move)
    pieces_eliminated = True
    while pieces_eliminated:
        pieces_eliminated = RemovePieces(board)
        DropPieces(board)
        FillBlanks(board)


@window.event
def on_draw():
    window.clear() # first clear window
    for i in range (7, -1, -1):# loop through the board and draw the corresponding image per piece
        # Draw each row
        y = 64+64*i # offset in x and y by 64 for every row and column since image is 64by64
        for j in range (8):
            # draw each piece, first getting position
            x = 64*j
            if board[i][j] == 'A':
                Im1.blit(x,y)
            elif board[i][j] == 'B':
                Im2.blit(x,y)
            elif board[i][j] == 'C':
                Im3.blit(x,y)
            elif board[i][j] == 'D':
                Im4.blit(x,y)
            elif board[i][j] == 'E':
                Im5.blit(x,y)
        label = pyglet.text.Label('Turn: '+str(turn)+'   Score: '+str( score),
                                  font_name = 'Arial',
                                  font_size = 18,
                                  x = 20, y = 10)
        label.draw()

### getting a move
# To make a move the user will click on one square, and then try to drag it to an adjacent cell and release
@window.event
def on_mouse_press(x, y, button, modifiers): # store the starting point of the square when they click the mouse
    # Get the starting cell
    global startx
    global starty
    startx = x
    starty = y

####### FINDING THE PLACE WHERE THE PLAYER RELEASES THE MOUSE.
@window.event
def on_mouse_release(x, y, button, modifiers):
    #get starting and ending cell and see if they are adjacent
    #since every grid cell takes up 64 pixels, we divide by 64 to get the position in the grid
    startcol = startx//64
    startrow = (starty - 64)//64
    endcol = x//64
    endrow = (y-64)//64 # since we left a gap at the bottom of the screen to write the score
    # Check whther ending is adjacent to starting and if so, make move (We need to have the same row with the column different by exactly 1, or the same col with the row different by 1)
    if ((startcol == endcol and startrow == endrow - 1) or (startcol == endcol and startrow == endrow+1) or (startrow == endrow and startcol == endcol-1) or (startrow == endrow and startcol == endcol+1)):
        Update(board, [startrow, startcol, endrow, endcol])
        global turn
        turn += 1
        # See if game is over
        if not ContinueGame(score):
            print("You won in", turn, "turns!")
            exit()


    
# State main variables
score = 100
turn = 100
goalscore = 100

board = [[0, 0, 0, 0, 0, 0, 0, 0],
         [0, 0, 0, 0, 0, 0, 0, 0],
         [0, 0, 0, 0, 0, 0, 0, 0],
         [0, 0, 0, 0, 0, 0, 0, 0],
         [0, 0, 0, 0, 0, 0, 0, 0],
         [0, 0, 0, 0, 0, 0, 0, 0],
         [0, 0, 0, 0, 0, 0, 0, 0],
         [0, 0, 0, 0, 0, 0, 0, 0]]

## Initialize game
Initialize(board)

pyglet.app.run()
