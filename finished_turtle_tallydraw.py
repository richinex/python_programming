# -*- coding: utf-8 -*-
"""
Created on Sat Jan 25 21:52:27 2020

@author: Richard  Chukwu
"""
######### FINISHED TALLYdRAW DESIGN USING TURTLE ##################
from turtle import forward, backward, left, right, penup, pendown, pos, goto
def drawTally():
    left(90)
    forward(20)
    backward(20)
    right(90)
    shiftRight()
    
def shiftRight():
    penup()
    forward(5)
    pendown()

def drawSlash():
    #move up
    left(90)
    penup()
    forward(3)
    pendown()
    
    #draw slash
    startposition = pos()
    goto(startposition[0] - 25,
    startposition[1] + 14) # move -25 and +14 from the current position
    # return to start position
    penup()
    goto(startposition)
    backward(3)
    right(90)
    pendown()
    
    #shift over
    shiftRight()
    shiftRight()

 
def drawFive():
    drawTally()
    drawTally()
    drawTally()
    drawTally()
    drawSlash()

def drawTallies(n):
    while(n>=5):
        drawFive() # groups of five tallies until we cant draw any more
        n = n - 5
    while(n>=1):
        drawTally()
        n = n - 1

num_to_draw = int(input("Enter a number: "))
drawTallies(num_to_draw)

input()
