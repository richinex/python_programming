# -*- coding: utf-8 -*-
"""
Created on Sat Jan 25 20:42:46 2020

@author: Richard Chukwu
"""
#####BOTTOM UP DESIGN, TURTLE GRAPHICS, ROBOTICS ######################
# In bottom-up approach you start with pieces of code that you already know how to use to build up towards more complex projects...flower, butter, eggs, I can bake so I can make a dessert. This lets you reuse ideas.

# Bottom-Up Design:
    # lets you re-use ideas
    # Can create new functionality
    # Yields time savings in developing new code
# works when we understand our building blocks. eig Siri (iphone) and Voice controlled digital assistants. we already had
    # Voice recognition software
    # Ability to run search
    # Convert text to speech
# Another approach where the bottom up approoach works well is robotics

################## CREATING TURTLE GRAPHICS #########################   
# Creating a square spiral

from turtle import forward, left # import forward and left commands

for i in range (1, 100):
    forward(2*i) # move forward and then,
    left(90) # turn left in degrees
    
input()

# Computer graphics go anticlockwise direction so that cross products between convex edges defined using the "right hand rule", will generally point up

from turtle import forward, backward, left, right, penup, pendown

forward(100)
left(90)
forward(100)
left(90)
forward(100)
left(90)
forward(100)

input()

## We could define our own function draw square
from turtle import forward, backward, left, right, penup, pendown

def drawSquare():
    forward(100)
    left(90)
    forward(100)
    left(90)
    forward(100)
    left(90)
    forward(100)
    left(90)
    
drawSquare()

input()

### Adding a size parameter
from turtle import forward, backward, left, right, penup, pendown

def drawSquare(size = 100):
    forward(size)
    left(90)
    forward(size)
    left(90)
    forward(size)
    left(90)
    forward(size)
    left(90)
    
drawSquare(50)

# Draw Triangle
from turtle import forward, backward, left, right, penup, pendown

def drawTriangle(size = 100):
    forward(size)
    left(120)
    forward(size)
    left(120)
    forward(size)
    left(120)
    
drawTriangle(50)

# Draw Rectangle
from turtle import forward, backward, left, right, penup, pendown

def drawRectangle(length = 100, height = 100):
    forward(length)
    left(90)
    forward(height)
    left(90)
    forward(length)
    left(90)
    forward(height)
    left(90)
    
drawRectangle(80, 60)


### Draw House
    # Draw a square
    # Move turtle to top of square
    # Draw a triangle
from turtle import forward, backward, left, right, penup, pendown
def drawHouse(size = 100):
    # draw base
    drawSquare(size)
    
    #reposition turtle
    left(90)
    forward(size)
    right(90)
    
    # draw top
    drawTriangle(size)
    
    # move back to start
    right(90)
    forward(size)
    left(90)
    
drawHouse(90)

#### CONSTRUCTING A TALLY MARK ###############
from turtle import forward, backward, left, right, penup, pendown
def drawTally():
    left(90)
    forward(20)
    backward(20)
    right(90)
    
drawTally()

input()

### How do we move to the next line
drawTally()
penup()
forward(5)
pendown()
drawTally()

input()  # We define all these as shiftRight

def shiftRight():
    penup()
    forward(5)
    pendown()

### How do we create the slash
def drawSlash():
    #move up
    left(90)
    penup()
    forward(3)
    pendown()
    
    #draw slash
    startposition = pos()
    goto(startposition[0] - 25),
    starposition[1] + 14
    # return to start position
    penup()
    goto(startposition)
    backward(3)
    right(90)
    pendown()
    
    #shift over
    shiftRight()
    shiftRight()
    
############ ROBOT VACUUM PROGRAM #################
# Basic turtle commands to govern robot's path
# Sensor function that will define if the robot is too close to a wall or obstacle. Sould return true if we are too close to an obstacle
    
from turtle import *
import random

xmax  = 250
xmin = -250
ymax = 250
ymin = -250
proximity = 10

def sensor():
    if xmax - position()[0] < proximity:
        # Too close to right wall
        return True
    if position()[0] - xmin < proximity:
        # Too close to left wall
        return True
    if ymax - position[1] < proximity:
        # too close to top wall
        return True
    if position()[1] - ymin < proximity:
        # Too close to bottom wall
        return True
    # Not too close to any
    return False

### Straightline function
    # Use turtle commands to pick random direction
    # Head in that direction until sensor is triggered
    # Random module can pick a new direction
def straightline():
    '''Move in a random direction until sensor is triggered'''
    # Pick a random direction
    left(random.randrange(0,360))
    # Keep going forward until a wall is hit
    while not sensor():
        forward(1)
        
### Spiral function
def spiral(gap = 20):# gap tells us how tight the spiral should be
    '''Move in a spiral with spacing gap'''
    # Determine starting radius of spiral based on the gap
    current_radius = gap
    while not sensor():
        # Determime how much of the circumference 1 unit is
        circumference = 2 * 3.142 * current_radius
        fraction = 1/circumference # move one unit along the circle's circumference
        # Move as if in a circle of that radius
        left(fraction*360) # the frac of 360 we want to turn
        forward(1)
        # Change radius so that we will be out by 2*proximity after 360 degrees
        current_radius += gap*fraction # increase radius with every step
    
### Wall-following function
        # Set direction parallel to closest wall
        # Use turtle function "setheading". setheading allows us give a direction 90 is north, 0 is east, 270 is south, and 180 is west
        
def followwall():
    '''Move turtle parallel to nearest wall for amount distance'''
    # find nearest wall and turn parallel to it
    min = xmax - position()[0] # store dist to the wall in min
    setheading(90)
    if position()[0] - xmin < min: # if wall is closer to the slosest one so far set new heading
        min = position()[0] - xmin
        setheading(270)
    if ymax - position()[1] < min:
        min = ymax - position()[1]
        setheading(180)
    if position()[1] - ymin < min:
        setheading(0)
        
    # Keep going until hitting another wall
    while not sensor():
        forward(1)
        
# Backward Spiral: Move backward for some amount and then spiral outward
def backupspiral(backup = 100, gap = 20):
    """First move backward by amount backup, then in a spiral with spacing gap"""
    # first back up by back up amount
    while not sensor() and backup > 0:
        backward(1)
        backup -+ 1
        #Determine starting radius of spiral based on the gap
        spiral(gap)
    
# Upon proximity sensorm move backwards before spiraling
speed(0) # tell the turtle to move as fast as possible
#start with a spiral
spiral(40)
while(True): # means we are close to a wall
    #First back up so no longer colliding
    backward(1)
    # Pick one of the three behaviors at random
    which_function = random.choice(['a', 'b', 'c'])
    if which_function == 'a':
        straightline()
    if which_function == 'b':
        backupspiral(random.randrange(100,200), random.randrange(10,50))# one  for back up and one for gap
    if which_function == 'c':
        followwall(random.randrange(100, 500))