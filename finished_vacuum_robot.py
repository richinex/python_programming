# -*- coding: utf-8 -*-
"""
Created on Sat Jan 25 23:30:27 2020

@author: User
"""

############ ROBOT VACUUM PROGRAM #################
# Basic turtle commands to govern robot's path
# Sensor function that will define if the robot is too close to a wall or obstacle. Sould return true if we are too close to an obstacle
    
from turtle import *
import random

xmax  = 250
xmin = -250
ymax = 250
ymin = -250
proximity = 10

def sensor():
    if xmax - position()[0] < proximity:
        # Too close to right wall
        return True
    if position()[0] - xmin < proximity:
        # Too close to left wall
        return True
    if ymax - position()[1] < proximity:
        # too close to top wall
        return True
    if position()[1] - ymin < proximity:
        # Too close to bottom wall
        return True
    # Not too close to any
    return False

### Straightline function
    # Use turtle commands to pick random direction
    # Head in that direction until sensor is triggered
    # Random module can pick a new direction
def straightline():
    '''Move in a random direction until sensor is triggered'''
    # Pick a random direction
    left(random.randrange(0,360))
    # Keep going forward until a wall is hit
    while not sensor():
        forward(1)
        
### Spiral function
def spiral(gap = 20):# gap tells us how tight the spiral should be
    '''Move in a spiral with spacing gap'''
    # Determine starting radius of spiral based on the gap
    current_radius = gap
    while not sensor():
        # Determime how much of the circumference 1 unit is
        circumference = 2 * 3.142 * current_radius
        fraction = 1/circumference # move one unit along the circle's circumference
        # Move as if in a circle of that radius
        left(fraction*360) # the frac of 360 we want to turn
        forward(1)
        # Change radius so that we will be out by 2*proximity after 360 degrees
        current_radius += gap*fraction # increase radius with every step
    
### Wall-following function
        # Set direction parallel to closest wall
        # Use turtle function "setheading". setheading allows us give a direction 90 is north, 0 is east, 270 is south, and 180 is west
        
def followwall(min):
    '''Move turtle parallel to nearest wall for amount distance'''
    # find nearest wall and turn parallel to it
    min = xmax - position()[0] # store dist to the wall in min
    setheading(90)
    if position()[0] - xmin < min: # if wall is closer to the slosest one so far set new heading
        min = position()[0] - xmin
        setheading(270)
    if ymax - position()[1] < min:
        min = ymax - position()[1]
        setheading(180)
    if position()[1] - ymin < min:
        setheading(0)
        
    # Keep going until hitting another wall
    while not sensor():
        forward(1)
        
# Backward Spiral: Move backward for some amount and then spiral outward
def backupspiral(backup = 100, gap = 20):
    """First move backward by amount backup, then in a spiral with spacing gap"""
    # first back up by back up amount
    while not sensor() and backup > 0:
        backward(1)
        backup -= 1
        #Determine starting radius of spiral based on the gap
        spiral(gap)

# Upon proximity sensorm move backwards before spiraling
speed(0) # tell the turtle to move as fast as possible
#start with a spiral
spiral(40)
while(True): # means we are close to a wall
    #First back up so no longer colliding
    backward(1)
    # Pick one of the three behaviors at random
    which_function = random.choice(['a', 'b', 'c'])
    if which_function == 'a':
        straightline()
    if which_function == 'b':
        backupspiral(random.randrange(100,200), random.randrange(10,50))# one  for back up and one for gap
    if which_function == 'c':
        followwall(random.randrange(100, 500))