# -*- coding: utf-8 -*-
"""
Created on Mon Feb  3 21:14:16 2020

@author: User
"""
import os
import shutil
classes = ('Bells', 'Cello', 'Clarinet', 'Crotales', 'Double_Bass ', 'Flute', 'Piano', 'Saxophone', 'Trombone', 'Trumpet', 'Vibraphone', 'Viola', 'Violin', 'Xylophone')

# create sub-folders for each class
OUTPATH = 'spectrogram_10/'
for x in classes:
    os.makedirs(OUTPATH+'train/'+x, exist_ok=True)
    os.makedirs(OUTPATH+'val/'+x, exist_ok=True)

INPATH = 'spectrograms/'
filenames = os.listdir(INPATH)
counts = {x:0 for x in classes}
print(len(filenames))

# copy files from cifar folder to cifar10 folder with sub-directories
valsz = len(filenames) / 10 * 0.2 # 20%

for fl in filenames:
    for cl in classes:
        if cl in fl:
            counts[cl] += 1 # increase count +1
            if counts[cl] < valsz:
                shutil.copy(INPATH+fl, OUTPATH+'val/'+cl+'/'+fl)
            else:
                shutil.copy(INPATH+fl, OUTPATH+'train/'+cl+'/'+fl)
       
# copy test set
filenames = os.listdir(INPATH+'test/')
os.makedirs(OUTPATH+'test/', exist_ok=True)

for fl in filenames:
    shutil.copy(INPATH+'test/'+fl, OUTPATH+'test/'+fl)