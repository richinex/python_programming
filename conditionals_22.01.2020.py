# -*- coding: utf-8 -*-
"""
Created on Wed Jan 22 01:40:08 2020

@author: Richard
"""
# a conditional helps us make a decision on what to choose. also called branching

if True:
    print("Turning on the heater") #in python a constant value is always capitalized
    # always be consisten about using four spaces
    # if, condition, semicolonm indented statement
    print("It was too cold")
    
# If-the-else statements
# have two sets of code to execute;
# one set if the condition is true;
# one set if the condition is false

if True:
    print("Turning on the heater")
else:
    print("Temperature is fine")
    
# indented conditionals (nesting)
if False:
    print("It is cold")
    if True:
        print("The heater is already on")
    else:
        print("Turning on the heater")

else:
    print("It is wrm enough") # in false the first chunk is not evaluated
    
# using a boolean
temp_is_low = True
if True:
    print("Turning on the heater")
else:
    print("Temperature is fine")
    
x = (age <= 12) # assume age is already define

if x:
    print("Your meal is free!)
else:
    print("Sorry, you have to pay.")
    
if age <= 12:
    print("Your meal is free!)
else:
    print("Sorry, you have to pay.")
    
    
# check if a number is between 1 and 10
if a >= 1:
    if a <= 10:
        print("It is in range")
    else:
        print("It is not in range")
else:
    print("It is not in range")
    
# if you dont combine your conditional with a boolean then you have to nest
if (a <= 10) and (a <= 10):
    print("It is in range")
else:
    print("It is not in range")   
    
# Else if
if (age < 3):
    price = 0.00
elif (age <= 12):
    price = 10.00
elif (age >= 65):
    price = 15.00
else:
    price = 25.00