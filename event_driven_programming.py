# -*- coding: utf-8 -*-
"""
Created on Sun Jan 26 12:17:12 2020

@author: Richard Chukwu
"""

############ EVENT DRIVEN PROGRAMMING #####################
##### HOW TO WRITE A GUI ##############
### Pyglet is a python package created to help support development of games and other audio-visual environments. It provides functions that let you create windows, display images and graphics, play videos and music, get input from mouse, etc. There is also pyglet.
#python -m pip install pyglet

# What we have been doing all the while is sequential programming
# In event driven programming, events outside the program determine what the program does next.
# An even is anything that happens where we want the program to respond
# Pressing a key on the key board
# Clicking a botton on the screen
# Entering data into a box on the screen
# Or moving a mouse
# Data from sensors act like events.
# Idle function or event monitor
# An event monitor uses interrupts or polls the various devices the type of events.
# Upon getting the event, the event monitor calls the function required to handle the event. This function is known as the event handler.
# The even handler takes in events and then calls what is known as a callback function corresponding to that event.
# The call back function is a function to execute in response to the event, at which time we go back to the event handler to get the next event. We say the event handler is "invoking" the call back function.

### Steps to writing an event driven program ####
## Callbacks muct be defined on their own. ready to handle their own time of parameter. like a calll back to take in keyboard strokes
## Initialization just like in the sequential programming.
    # Set up any variables, data, etc.
    # register callbacks: this lis like saying what function will be called for each event that we want to respond to. So we have to write functions for each possible event.
    # Start up the event monitor. Once started it keeps running indefinitely.
    
# here we use a basic pyglet program.
    
import pyglet

window = pyglet.window.Window(width=400, height=300, caption="TestWindow")

pyglet.app.run() #starts the event handler

# Above we have just created an empty window. Now lets create some text in what is known as a label.(i.e a label is text printed using the pyglet module)

import pyglet

window = pyglet.window.Window(width=400, height=300, caption="TestWindow")

label1 = pyglet.text.Label('Howdy Richard',
                          font_name = 'Comic Sans',
                          font_size = 18,
                          x = 50, y = 150)# in pixels

label2 = pyglet.text.Label('You are doing just great!',
                          font_name = 'Arial',
                          font_size = 18,
                          x = 50, y = 100)# in pixels
# Ways to get position values in pyglet
# in pyglet, position is given in terms of the number of pixels from the lower left corner, with x in the horizontal direction and y in the vertical. In other graphic frameworks, it is often common to start from the upper left hand corner, and have y be the distance down the window. Aso, sometimes, x and y are not specified in pixels, but rather in values relative to the current width and height, like floating points between 0 and 1.

@window.event # first call back whenever we open a window. i.e we are about to respond to an event
def on_draw(): # function to be called when a draw even occurs in the window. the code opens a window, clears it and draws our label.
    window.clear()
    label1.draw()
    label2.draw()
    
    
pyglet.app.run() #starts the event handler

### registering a keyboard event. This even is triggered when a key is pressed down, not because something is typed in. So we need a call back to handle on key presse
import pyglet

window = pyglet.window.Window(width=400, height=300, caption="TestWindow")

label = pyglet.text.Label('Nothing pressed so far',
                          font_name = 'Comic Sans',
                          font_size = 18,
                          x = 50, y = 150)# in pixels

@window.event
def on_key_press(symbol, modifiers):#symbol gives the key pressed. modifier is a key help down with another key, ctrl, shift
    global label
    label = pyglet.text.Label('You pressed a key',
                              font_name = 'Times New Roman',
                              font_size = 18,
                              x = 50, y = 150)

@window.event 
def on_draw(): 
    window.clear()
    label.draw()
    
    
    
pyglet.app.run()

### Specifying a sumbol key
@window.event
def on_key_press(symbol, modifiers):#symbol gives the key pressed. modifier is a key help down with another key, ctrl, shift
    if symbol == pyglet.window.key.A:# to refrain typing this long syntax, import key from pyglet.window
        key_pressed = "a"
    else:
        key_pressed = 'unknown'
    global label
    label = pyglet.text.Label('You pressed a key',
                              font_name = 'Times New Roman',
                              font_size = 18,
                              x = 50, y = 150)
#### Importing key
    # return = RETURN, left = LEFT
import pyglet
from pyglet.window import key

window = pyglet.window.Window(width=400, height=300, caption="TestWindow")

label = pyglet.text.Label('Nothing pressed so far',
                          font_name = 'Comic Sans',
                          font_size = 18,
                          x = 50, y = 150)# in pixels

@window.event
def on_key_press(symbol, modifiers):#symbol gives the key pressed. modifier is a key help down with another key, ctrl, shift
    if symbol == key.A:# to refrain typing this long syntax, import key from pyglet.window
        key_pressed = "a"
    elif symbol == key.RETURN:
        key_pressed = "return"
    elif symbol == key.LEFT:
        key_pressed = "left"
    else:
        key_pressed = "unknown"
    global label
    label = pyglet.text.Label("You pressed the "+key_pressed+" key!",
                              font_name = 'Times New Roman',
                              font_size = 18,
                              x = 50, y = 150)
    
@window.event 
def on_draw(): 
    window.clear()
    label.draw()
    
    
    
pyglet.app.run()

### Mouse Event handling ###########

import pyglet

window = pyglet.window.Window(width=400, height=300, caption="TestWindow")

label = pyglet.text.Label('Nothing pressed so far',
                          font_name = 'Comic Sans',
                          font_size = 18,
                          x = 50, y = 150)# in pixels
@window.event
def on_mouse_press(x, y, button, modifiers):#mouse pressed down,
    global label
    label = pyglet.text.Label("Mouse click at position ("+str(x)+", "+str(y)+")",
                              font_name = "Times New Roman",
                              font_size = 18,
                              x = 50, y = 150)

    
@window.event 
def on_draw(): 
    window.clear()
    label.draw()
    
    
    
pyglet.app.run()

## On mouse position ###
import pyglet

window = pyglet.window.Window(width=400, height=300, caption="TestWindow")

label = pyglet.text.Label('Nothing pressed so far',
                          font_name = 'Comic Sans',
                          font_size = 18,
                          x = 50, y = 150)# in pixels
@window.event
def on_mouse_motion(x, y, dx, dy):#mouse pressed down,gives the change in position
    global label
    label = pyglet.text.Label("Mouse click at position ("+str(x)+", "+str(y)+")",
                              font_name = "Times New Roman",
                              font_size = 18,
                              x = 50, y = 150)

    
@window.event 
def on_draw(): 
    window.clear()
    label.draw()
    
pyglet.app.run()

## On mouse release
import pyglet

window = pyglet.window.Window(width=400, height=300, caption="TestWindow")

label = pyglet.text.Label('Nothing pressed so far',
                          font_name = 'Comic Sans',
                          font_size = 18,
                          x = 50, y = 150)# in pixels
@window.event
def on_mouse_release(x, y, button, modifiers):#mouse pressed down,
    global label
    label = pyglet.text.Label("Mouse click at position ("+str(x)+", "+str(y)+")",
                              font_name = "Times New Roman",
                              font_size = 18,
                              x = 50, y = 150)

    
@window.event 
def on_draw(): 
    window.clear()
    label.draw()
    
pyglet.app.run()

### On mouse drag ###
import pyglet

window = pyglet.window.Window(width=400, height=300, caption="TestWindow")

label = pyglet.text.Label('Nothing pressed so far',
                          font_name = 'Comic Sans',
                          font_size = 18,
                          x = 50, y = 150)# in pixels
@window.event
def on_mouse_release(x, y, dx, dy, button, modifiers):#mouse pressed down,
    global label
    label = pyglet.text.Label("Mouse click at position ("+str(x)+", "+str(y)+")",
                              font_name = "Times New Roman",
                              font_size = 18,
                              x = 50, y = 150)

    
@window.event 
def on_draw(): 
    window.clear()
    label.draw()
    
pyglet.app.run()

############## USING IMAGES ##############
## Reading in an images and displaying images
#pyglet.image.load() #reads in an image to python

import pyglet

window = pyglet.window.Window(width=400, height=300, caption="TestWindow")

Im1 = pyglet.image.load('blutri.jpg')

    
@window.event 
def on_draw(): 
    window.clear()
    Im1.blit(50,50)#blit means block image transfer which refers to whenever two bitmaps are combined together
    
pyglet.app.run()

#### Besides pyglet, we have the tkinter Module
# Useful for creating graphical user interfaces with buttons, boxes, and sliders. It is a corss-platform toolkit that has been ported to many programming languages.
# It enables python programmers have relatively easy access to a powerful cross-platform GUI library. It relies on OOP

######### OBJECT ORIENTED PROGRAMMING INTRO WITH TKINTER ####
import tkinter

class Application(tkinter.Frame): # classes are a way of grouping things together in OOP. This class inheritss from tkinter.Frame. The indented stuff describes our window and how it works
    def __init__(self, master = None):
        tkinter.Frame.__init__(self, master)#this object sets up the shape of the window
        self.pack() # lets tkinter pack the widgets into the window
        
        self.increase_button = tkinter.Button(self)# widgets. first we create a button object which contains data called attributes and functions called methods
        self.increase_button["text"] = "Increase" # text attribute
        self.increase_button["command"] = self.increase_value # the command attribute says when the button is pressed, we should call the increase value function
        self.increase_button.pack(side = "right") # calls the pack method to place button on the right
        
        self.increase_button = tkinter.Button(self)
        self.increase_button["text"] = "Decrease"
        self.increase_button["command"] = self.decrease_value
        self.increase_button.pack(side = "left")
        
    def increase_value(self):# takes mainval, multiplies it by 2 and prints the new value to the output window
        global mainval # global value
        mainval *=2
        print (mainval)
    def decrease_value(self):
        global mainval
        mainval /= 2
        print(mainval)
        
mainval = 1.0

##To start the event manager
root = tkinter.Tk() # sets up tk
app = Application(master = root) #class defines our window
app.mainloop() #starts the event manager

## Both Pyglet and tkinter
    # Both have event handlers that will run until an even occurs
    # Both have functions that are registered to the different event
    # both support graphic display. Tk specifically for interactive windows with widgets while pyglet for images and 3D graphics