# -*- coding: utf-8 -*-
"""
Created on Thu Jan 23 16:44:23 2020

@author: Richard
"""

# While loop: an if-then statement that is repeated over and over. Used when we are not sure of how many times we'll need to iterate through the loop
while value <= 0:
    value = int(input("Enter a positive value!"))
    print("You entered", value)

alive = True    
while alive:
    print("Always look on the bright side of life!")
    
# another example of an infinite loop because we only check the condition at the end of the body
value = 3
while value > 0:
    print(value)
    value = -3
    print(value)
    value = 7
    
# finite loop
a = 0
while a < 3:
    print(a)
    a = a+1
    
# one time through a loop is an iteration.
    
# count down finite loop
value = 10
while value > 0:
    print(value)
    value -= 1
    
# write a loop for counting the ages of all the people in a group
num_people = int(int(input("How many people are there? ")))#ask the user
i = 0 # initialize the counter to zero. This is the iterator
total_age = 0.0
while (i < num_people):
    age = float(input("Enter the age of person" +str(i+1)+ ": "))
    total_age = total_age + age
    i = i + 1
average_age = total_age / num_people
print("The average age was", average_age)


# For loops
#Type of loop where a variable takes on a new value in sequence on every iteration through the loop. Used when we have a precise set of things to loop through, or know how many times to iterate.
# the two loops do the same thing
for i in range(5,1,-1):
    print(i)
    
i = 5
while i > 1:
    print(i)
    i = i-1
    
# the code below keeps asking for a value until we enter a positive number:
while value <= 0:
    value = int(input("Enter a positive value!"))
else:
    print("You entered", value)
    
# the code to print multiplication tables
for i in range(12):
    for j in range(12):
        print(i, "*", j, "=", i*j)
        
# the code to print multiplication tables (omit the zeros)
for i in range(12):
    for j in range(12):
        print(i+1, "*", j+1, "=", (i+1)*(j+1))
        
# Pairing program
# print a list of every possible pair of people
# only want each pair once (A-B is the same as B-A)
numpeople = int(input("How many people are there?"))
for i in range(1, numpeople+1): #this ensure we start at 1 and include numpeople
    for j in range(i+1, numpeople+1):#we need to pair i in original loo[ with i+1
        print(i, j)
        
# using the continue statement to skip the rest of the body and go to the end
day_to_skip = 4
hours_worked = 0
target_hours = 10
day = 0
while hours_worked < target_hours:
    day += 1
    target_hours += 8
    if day_to_skip == 0:
        continue # im done lets skip to the end
    if day < 10:
        hours_worked += 6
    elif day < 15:
        hours_worked += 8
    else:
        hours_worked += 14
        
# Break command: exits the loop immediately, skipping anything else in the loop. More extreme version of continue. You even skip the else part of the loop. to get out of a loop when you know you might encounter a problem. If you have nested loops, the will only let you break out of, or continue within, the innermost loop
bad_number  = 7
for i in range(1, 10, 2):
    if i == bad_number:
        print("Whoops! We didnt want that number!")
        break
    print(i)
else:
    print("We got through all numbers")
    
# Think of loops:
# Use to get a bunch of input, or a lot of output, or just do any action over and over
    
# Collatz Conjecture (3N + 1)
# Given some number, there is a pair of manipulations that can be repeated until it eventually will reach 1.
# get a starting number, then form a sequence by these rules
# Given the last number, do the following to get the next number:
    # If the last number is even, divide it by two
    # if the last number is odd, multiply it by 3 and add 1
# stop when the number 1 is reached

n  = int(input("Enter a number: "))
print(n)
count = 0
while n != 1:
    count += 1
    if n % 2 == 0:
        n = n/2
    else:
        n = 3 * n+1
    print(n)
print("We reached 1 in", count, "steps.")