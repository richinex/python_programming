# -*- coding: utf-8 -*-
"""
Created on Sat Jan 25 11:27:44 2020

@author: Richard
"""

# Modules: Functions, or sets of functions that are pre-bundled together (batteries included). A program that supports abstraction.
# Packages: Bundles of modules

# You can use an "import" command to load one program a sa module for use in another program.

# some modules aid us in basic programming functionality like math 9standard library) and numpy
import math
print(math.sin(math.pi/2))

# Some let us communicate over networks like
import ssl

# Some can open a browser window like
import webbrowser
webbrowser.open("http://www.thegreatcourses.com")

# Some allow us to draw graphics to the screen
import turtle
turtle.forward(100)

# For GUIs
import Tkinter
# links to Tcl/Tk library for GUIs

# Some let us do things on the computer itself
import shutil
shutil.copy("File1.txt", "File2.txt") # lets us copy from file 1 to file 2

help("modules") # to see how many modules installed

#import moduleA....works like source in R to import functions.
# Modules:
# Define a set of functions we can use later in our programs
# Define variables
# Define classes
# Can be used in different programs with the import() command

# For example using the function sumSquares we defined
import sumSquares
print(sumSquares.SumofSquares(3))

# Why do we include the module? it makes it possible for our program to distinguish reliably between that might have the same name imported from two different modules. This is a good thing. 
# If you dont want to type in the module name always we use the word from

from sumSquares import SumofSquares
print(SumofSquares(3))

# What really raises a program to the level of a module is that the program provides a set of functions, classes and constants that all work together to accomplish a common goal.
# We have the Python Standard Library modules
# To import all the functions, we use the star *
from math import *
print(cos(pi/4.0))

import calendar
calendar.setfirstweekday(calendar.SUNDAY)
print(calendar.month(2020, 1))

# For a variety of time functions we use the time module
import time
print(time.ctime()) # gets the current date and time

import statistics
a = [3, 10, 23, 5, 14, 83, 7, 1]
print(statistics.median(a))

import shutil
shutil.copy("temperature.dat", "weather.txt") # allows you mess with files. here we copy temperature.dat into weather.txt


# And Third-party publicly-distributed modules
# Dont forget to add the init file. An extra period allows us access the inner modules
import MyFavoritePrograms.sumSquares
print(MyFavoritePrograms.sumSquares.SumofSquares(3))

# Examples include:
# Numpy/Scipy for mathematical and scientific computing
# Matplotlib for graphing and plotting capabilities
# Zero MQ for messaging
# Twisted for networking
# BeautifulSoap helps to process HTML files
# Requests allows getting data files over the web

### How to find a good module
# Browse through the Python Package Index (PyPI)
# https://pypi.python.org/pypi   "Official" index of third-party Python module releases. Use the ranking to determine popularity. The recommended tool for installing packages is the pip install from the command line
#python -m pip install <package name>

import math
print(dir(math))
help(trunc) # help identifies the information in the doc tag

# Suppose we wanna take all the files with some extension in a directory, and rename them with some new name, followed by a number, we use the OS module that can manipulate files in an operating system.

import os

dirname = input("Enter the directory: ") # directory to rename
os.chdir(dirname) # we change the working directory
dirlist = os.listdir() # we list the files and directories in directory


picture_number = 1
for filename in dirlist:
    if filename.endswith(".jpg"): #we check if the sile ends with jpg
        newname = "SummerVacation"+str(picture_number)+".jpg"
        os.rename(filename, newname)
        picture_number += 1
# Whenever we specify a specifc value like summer vacation instead of a variable, it is called Hardcoding the value. More convenient in the short term. Softcoding is more time consuming due to debugging but allows flexibility
        
# letting the user chose the new name
        dirname = input("Enter the directory: ") # directory to rename
os.chdir(dirname) # we change the working directory
dirlist = os.listdir() # we list the files and directories in directory

lead = input(" what label do you want for the pictures?")


picture_number = 1
for filename in dirlist:
    if filename.endswith(".jpg"): #we check if the sile ends with jpg
        newname = "lead"+str(picture_number)+".jpg"
        os.rename(filename, newname)
        picture_number += 1
        
# Remeber modules have a learning curve. identify the particular task and find the modules to help you.