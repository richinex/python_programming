
import turtle
turtle.screensize(400,300)
turtle.bgcolor("black")
turtle.speed(0)
turtle.color("red", "pink")

def drawLine():
    turtle.pensize(2)
    turtle.left(90)
    turtle.forward(170)
    turtle.backward(170)
    turtle.right(90)
    shiftRight()
    
def shiftRight():
    turtle.penup()
    turtle.forward(140)
    turtle.pendown()




def curve():
    for i in range(200):
        turtle.right(1)
        turtle.forward(1)
  

def drawHeart():
    # draw left part of heart
    turtle.pensize(2)
    turtle.begin_fill()
    turtle.left(140)
    turtle.forward(111.65)
    curve()

    ## draw right part of heart
    turtle.left(120)
    curve()
    turtle.forward(111.65)
    turtle.end_fill()
    turtle.left(140)
    #turtle.hideturtle()
    
  
def drawU():
    turtle.pensize(2)
    turtle.left(90)
    turtle.penup()
    turtle.delay(100)
    turtle.forward(170)
    turtle.pendown()
    turtle.backward(120)
    turtle.delay(100)
    turtle.circle(-50, -165)
    turtle.left(195)
    turtle.delay(100)
    turtle.forward(130)
    turtle.hideturtle()

    
drawLine()    
drawHeart()
shiftRight()

drawU()
   
turtle.exitonclick()
