# -*- coding: utf-8 -*-
"""
Created on Sun Jan 26 22:57:03 2020

@author: Richard Chukwu
"""
#### VISUALIZING DATA AND CREATING SIMULATIONS ###########

##Matplotlib: PPython package for creating plots, graphs, and charts form data.

from matplotlib import pyplot
pyplot.axes() # means we are gonna have a data plot that uses axes

pyplot.show()

### we can supply a list for the axes
pyplot.plot([0, 1, 2, 3, 4, 5], [0, 1, 4, 9, 16, 25])
pyplot.axis([0,5,0,25])# pass a paramater showing the limits of the x and y axes
pyplot.show()

## Another option
from matplotlib.pyplot import plot, axis, show
xlist = range(0,6)
ylist = []
for i in xlist:
    ylist.append(i*i)
plot(xlist, ylist)

axis([0,5,0,25])

show()

### Plot Improvement
    # Make the plot a red line
    # Each point marked with a blue cross
    # Label the graph "squares"
from matplotlib.pyplot import plot, axis, show, legend
xlist = range(0,6)
ylist = []
for i in xlist:
    ylist.append(i*i)
plot(xlist, ylist, label = "squares", marker = "+", color = "red", markeredgecolor = "blue")

axis([0,5,0,25])

legend()

show()

### Two plots in one
from matplotlib.pyplot import plot, axis, show, legend
xlist = range(0,6)
ylist1 = []
ylist2 = []
for i in xlist:
    ylist1.append(i*i)
    ylist2.append(i*i*i)
plot(xlist, ylist1, label = "squares", marker = "+", color = "red", markeredgecolor = "blue")
plot(xlist, ylist2, label = "squares", marker = "o", color = "green", markeredgecolor = "green")
axis([0,5,0,125])

legend()

show()

## Complicated plot
from matplotlib.pyplot import plot, axis, show, legend, subplots
fig, mainplot = subplots()
plot2 = mainplot.twinx()
plot3 = mainplot.twinx()
fig.subplots_adjust(right = 0.75)

plot3.spines["right"].set_position(("axes", 1.15))

p1, = mainplot.plot([2000, 2004, 2006, 2010], [350, 550, 900, 700], "b", label = "Flash")
p2, = plot2.plot([2000, 2009], [12, 17], "r", label = "Bears")
p3, = plot3.plot([2000, 2002, 2004, 2006, 2008, 2010], [11, 13, 15, 18, 24, 29], "g", label = "Eagles")

mainplot.set_xlim(2000, 2010)
mainplot.set_ylim(300, 1000)# here we have three different vertical axes
plot2.set_ylim(0, 20)
plot3.set_ylim(0, 30)

mainplot.set_xlabel("Year")
mainplot.set_ylabel("Fish")
plot2.set_ylabel("Bears")
plot3.set_ylabel("Eagles")

mainplot.yaxis.label.set_color("b")
plot2.yaxis.label.set_color("r")
plot3.yaxis.label.set_color("g")

mainplot.tick_params(axis="y", colors = "b")
plot2.tick_params(axis = "y", colors = "r")
plot3.tick_params(axis = "y", colors = "g")

lines = [p1, p3, p3]
mainplot.legend(lines, [i.get_label() for i in lines])

show()

#### Graph the amount of a mortgage over time
from matplotlib.pyplot import plot, axis, show, legend

mortgage_amount = float(input("How much is the mortgage for?"))
interest_rate = float(input("what is the interest rate (as a percentage)?"))/100
payment = float(input("How much are you paying per month?"))

max_months = 360 # don't figure for more than 30 years
month = 0
month_list = [0]
mortgage_list = [mortgage_amount]
principal_paid_list = [0]
interest_paid_list = [0]

while (mortgage_amount > 0.0) and (month < max_months): #reduce mortgage to zero
    month += 1
    month_list.append(month) # update month array with new month
    interest = mortgage_amount * (interest_rate/12) # amount of interest generate per month
    interest_paid_list.append(interest+interest_paid_list[month-1])
    # determine principal paid and remaining
    principal = payment - interest
    mortgage_amount -= principal
    mortgage_list.append(mortgage_amount)
    principal_paid_list.append(principal+principal_paid_list[month-1])
    
plot(month_list, mortgage_list, label = "Remaining Mortgage", color = "red")
plot(month_list, principal_paid_list, label = "Principal paid", color = "blue")
plot(month_list, interest_paid_list, label = "Interest paid", color = "green")
    
axis([0, month, 0, max(interest_paid_list[month], mortgage_list[0])])

legend()
show()

### SIMULATIONS ###################
# Simulations motivated the development of the earliest computers:
    # Havard's Mac 1 computer did simulations for the US navy in the manhattan project in WWII
    # ENIAC, the first fully electronic computer simulated ballistic trajectories for the US Army starting in 1946

# Two ideas usually mixed
    # A model tells what the laws, rules, or processes that we are trying to compute should follow
    # The actual simulation takes the model and some set of conditions, and uses it to determine how the situation develops, usually over time
    
### Comet Simulation
    # Model: gravity of the sun and the planets
    # initial condition: starting position and velocity of the comet
    # simulation: predicts where the comet will be in the future
    
# The model is the most important thing about the whole simulation process. If the model is wrong, like the model of gravity then the prediction is wrong. Having the initial conditions is also necessary. Tiny errors in the initial conditions can have enromous effects on the results.
    
 # This we have a model of behavior and an initial(condition) state S0, at initial time t0. the overall values that we are wanting to simulate are the states of the system. Target time is T and Timestep is h (heavside function)
# The idea is that we are going to take steps forward in time by that amount h. That will let us determine a new state at that new time. This sequence of states we call Si.
# Thus a simulation loop:
#while ti < T:
     #set of states Si and set of times ti
     
## EXAMPLE 1 ###########
# We wanna see how an account accumulates interest over time.
     #Suppose we use a $1000 to buy a 10-year certificate of deposit
     # That earns 3% per year
    
# model: Interest rate increases balance by 3% per year
# Initial state balance is $1000, at year 0
# Timestep h (1 year), and ending Time T(10 years)
# Simulation loop:
     #while ti < 10
# Set of states Si and set of times ti: Given balance at time ti, find new balance one year later (time ti + 1)
     
#########
from matplotlib.pyplot import plot, show
time = 0
balance = 1000
# Set list to store data
timelist = [time]
balancelist = [balance]

while(time < 10): # simulation loop
    # Increase balance and time
    balance += balance * 0.03
    time += 1
    # Store time and balance in lists
    timelist.append(time)
    balancelist.append(balance)
    
# output the simulation results
for i in range(len(timelist)):
    print("Year:", timelist[i], " Balance:", balancelist[i])

plot(timelist, balancelist)
show()     

## In scientif equations, the model is a set of differential equations. For this class we do montecarlo simulations
## Monte Carlo simulations are based on the idea of simulating lots of random events but doing it enough times that the overall outcome will be understandable.
#An MC approach is used in a host of situations from fluid physics to finance, and especially situations in which theres alot of uncertainty to include

### Applying MC to finance
# We create a separate function that will calculate how the investment will increase in any particular year
import random
from matplotlib.pyplot import plot, show

def ChangeInBalance(initial_balance):#takes in current bal and returns how much it changes a year later
    rate = random.uniform(0.0, 0.06)
    return initial_balance*rate

time = 0
balance = 1000
# Set list to store data
timelist = [time]
balancelist = [balance]

# suppose we have an investment(interest rate) that fluctuates but with a non-negative return, instead of increasing our balance by 3% each year, we might change the balance by a random percentage. A simple way to do this is to imagine that we could select a min and max levels and every rate in between is equally likely
while(time < 10): # simulation loop. 
    # Increase balance and time
    balance += ChangeInBalance(balance)
    time += 1
    # Store time and balance in lists
    timelist.append(time)
    balancelist.append(balance)
    
# output the simulation results
for i in range(len(timelist)):
    print("Year:", timelist[i], " Balance:", balancelist[i])

plot(timelist, balancelist)
show()     

#Every time we run the code we get a differentb result. Now over the 10 year period, it tends to come out pretty similar, since the variations will tend to avearge out. To get an even better sense of what the overall performance is likely to be, we can run this code multiple time. To do this we wrap all this in another loop. we get read of the balance kists and store only the final.

import random
from matplotlib.pyplot import hist, show
def ChangeInBalance(initial_balance):
    rate = random.uniform(0.0, 0.06)
    return initial_balance*rate

number_years = 10
number_sims = 10000
# Set list to store data
final_balances = []

for i in range(number_sims):
    #set initial conditions
    time = 0
    balance = 1000
    
    while(time < number_years): # simulation loop. 
        # Increase balance and time
        balance += ChangeInBalance(balance)
        time += 1
        # Store time and balance in lists
    final_balances.append(balance)
    
# output the simulation results
for i in range(number_sims):
    print("Final balance:", final_balances[i])

hist(final_balances, bins = 20)
show()
     
### Suppose tha instead of a random percentage between 0-6, you want to pick a random percentage from a normal bell curve distribution with an average of 3% and a standard deviation of 2%
import random
from matplotlib.pyplot import hist, show
def ChangeInBalance(initial_balance):
    rate = random.gauss(0.03, 0.02) # slightly faster than the normalvariate
    return initial_balance*rate

number_years = 10
number_sims = 10000
# Set list to store data
final_balances = []

for i in range(number_sims):
    #set initial conditions
    time = 0
    balance = 1000
    
    while(time < number_years): # simulation loop. 
        # Increase balance and time
        balance += ChangeInBalance(balance)
        time += 1
        # Store time and balance in lists
    final_balances.append(balance)
    
# output the simulation results in a histogram
hist(final_balances, bins = 20)
show()
     
## Say we wanted to simulate a stock fund, we take a historical list of yearly increases or decreases in the SAP 500 and just select one of those. We would need to load a list of yearly returns and randomly select one of the elements of the list.

import random
import statistics
from matplotlib.pyplot import hist, show
historical_stock_returns = [0.1348, 0.3215, 0.1589, 0.210, 0.1482, 0.2594, -0.3655, 0.548, 0.1561]

historical_bond_yields = [0.0485, 0.051, 0.0494, 0.0566, 0.0729, 0.0744, 0.0648, 0.0648, 0.0606]
def ChangeInBalanceStocks(initial_balance):
    rate = random.choice(historical_stock_returns) # slightly faster than the normalvariate
    return initial_balance*rate

def ChangeInBalanceBonds(initial_balance):
    rate = random.choice(historical_bond_yields)
    return initial_balance*rate
number_years = 10
number_sims = 10000
# Set list to store data
final_balances_stocks = []
final_balances_bonds = []

for i in range(number_sims):
    #set initial conditions
    time = 0
    balance_stocks = 1000
    balance_bonds = 1000
    
    while(time < number_years): # simulation loop. 
        # Increase balance and time
        balance_stocks += ChangeInBalanceStocks(balance_stocks)
        balance_bonds += ChangeInBalanceBonds(balance_bonds)
        time += 1
        # Store time and balance in lists
    final_balances_stocks.append(balance_stocks)
    final_balances_bonds.append(balance_bonds)
    
final_balance_average = statistics.mean(final_balances_stocks)
final_balance_standard_deviation = statistics.stdev(final_balances_stocks)
print("The average final balance for a stock account was", final_balance_average)
print("The standard deviation in the final balance for a bond account was was", final_balance_standard_deviation)
final_balance_average = statistics.mean(final_balances_bonds)
final_balance_standard_deviation = statistics.stdev(final_balances_bonds)
print("The average final balance for a bond account was", final_balance_average)
print("The standard deviation in the final balance for a bond account was", final_balance_standard_deviation)   
      
# output the simulation results in a histogram
hist([final_balances_stocks, final_balances_bonds], bins = 40)
show()

# We could use the statistics module
# different account types have different characteristics
# Stocks fluctuate a lot in price, but tend to do best on average
# bonds don't do as well as stocks on average, but drops as well as gains tend to be smaller
# Savings accounts fluctuate the least but have the worst average performance

#### For a mixed account we remodify our code.
import random
import statistics
from matplotlib.pyplot import hist, show
historical_stock_returns = [0.1348, 0.3215, 0.1589, 0.210, 0.1482, 0.2594, -0.3655, 0.548, 0.1561]

historical_bond_yields = [0.0485, 0.051, 0.0494, 0.0566, 0.0729, 0.0744, 0.0648, 0.0648, 0.0606]
def YearData():
    ''' Returns a rate of change for stocks, bonds, and inflation in a single tuple'''
    year = random.randrange(9)
    return (historical_stock_returns[year], historical_bond_yields[year])
  
number_years = 10
number_sims = 10000
# Set list to store data
final_balances_stocks = []
final_balances_bonds = []
final_balances_mixed = []

for i in range(number_sims):
    #set initial conditions
    time = 0
    balance_stocks = 1000
    balance_bonds = 1000
    balance_mixed_stocks = 500 # assume a 50:50 split
    balance_mixed_bonds = 500
    
    while(time < number_years): # simulation loop. 
        # Increase balance and time
        stock_perform, bond_perform = YearData()
        balance_stocks *= (1.0+stock_perform)
        balance_bonds *= (1.0+bond_perform)
        balance_mixed_stocks *= (1.0+stock_perform)#Each year we increase the mixed stocks and mixed bonds separately
        balance_mixed_bonds *= (1.0+bond_perform)
        # rebalance account
        balance_mixed = balance_mixed_stocks + balance_mixed_bonds # look at overall balance to see if we are outof balance
        target_stocks = balance_mixed * 0.5 # 50% of total
        amount_to_move = balance_mixed_stocks - target_stocks # the diff we need to move from stocks to bond or vice versa
        balance_mixed_stocks -= amount_to_move
        balance_mixed_bonds += amount_to_move
        time += 1
        # Store time and balance in lists
    final_balances_stocks.append(balance_stocks)
    final_balances_bonds.append(balance_bonds)
    final_balances_mixed.append(balance_bonds)
final_balance_average = statistics.mean(final_balances_stocks)
final_balance_standard_deviation = statistics.stdev(final_balances_stocks)
print("The average final balance for a stock account was", final_balance_average)
print("The standard deviation in the final balance for a bond account was was", final_balance_standard_deviation)
final_balance_average = statistics.mean(final_balances_bonds)
final_balance_standard_deviation = statistics.stdev(final_balances_bonds)
print("The average final balance for a bond account was", final_balance_average)
print("The standard deviation in the final balance for a bond account was", final_balance_standard_deviation)   
final_balance_average = statistics.mean(final_balances_mixed)
print("The average final balance for a 50-50 mixed account with rebalancing was", final_balance_average)     
# output the simulation results in a histogram
hist([final_balances_stocks, final_balances_bonds, final_balances_mixed], bins = 40)
show()

#### RETIREMENT SIMULATION #########
# You have a retirement account and you just retired and you wanna know if you will be able to live off that money. Uncertainties include
    #How much will the account earn
    # How much will you have to withdraw each year to keep your standard of living, accounting for inflation?
    # How many years does it need to last
import random
import statistics
from matplotlib.pyplot import hist, show
historical_stock_returns = [0.1348, 0.3215, 0.1589, 0.210, 0.1482, 0.2594, -0.3655, 0.548, 0.1561]

historical_bond_yields = [0.0485, 0.051, 0.0494, 0.0566, 0.0729, 0.0744, 0.0648, 0.0648, 0.0606]

historical_inflation = [0.0,0.016,0.015,0.021, 0.032, 0.016, -0.004, 0.028, 0.032]
def ChangeInBalanceStocks(initial_balance):
    rate = random.choice(historical_stock_returns) # slightly faster than the normalvariate
    return initial_balance*rate

def ChangeInBalanceBonds(initial_balance):
    rate = random.choice(historical_bond_yields)
    return initial_balance*rate

def Inflation():
    return random.choice(historical_inflation)

def YearData():
    ''' Returns a rate of change for stocks, bonds, and inflation in a single tuple'''
    year = random.randrange(9)
    return (historical_stock_returns[year], historical_bond_yields[year], historical_inflation[year]) 
initial_balance = float(input("What is your retirement account balance? "))
initial_expenses = float(input("What are your yearly expenses? "))
time_to_last = int(input("How many years does this need to last? "))

number_sims = 10000
# Set list to store data
final_balances = []
ran_out = 0  # will equal how many times the money ran out
stock_percentage = 0.5

for i in range(number_sims):
    #set initial conditions
    time = 0
    balance_stocks = initial_balance * stock_percentage
    balance_bonds = initial_balance * (1.0 - stock_percentage)
    expenses = initial_expenses
    
    while(time < time_to_last): # simulation loop.
        time += 1
        # Increase balance and time
        stock_perform, bond_perform, inflation = YearData()
        balance_stocks *= (1.0+stock_perform)
        balance_bonds *= (1.0+bond_perform)
        # rebalance account
        balance_mixed = balance_stocks + balance_bonds
        target_stocks = balance_mixed * stock_percentage # 50% of total
        amount_to_move = balance_stocks - target_stocks
        balance_stocks -= amount_to_move
        balance_bonds += amount_to_move
        # Remove this year's expenses, ehich increases by inflation
        expenses *= (1.0+inflation)
        balance_stocks -= expenses * stock_percentage
        balance_bonds -= expenses * (1.0+stock_percentage)
        
        if (balance_stocks < 0):
            #We ran out of money! Increase ran_out count and set time_to_last to stop loop
            ran_out += 1
            time = time_to_last

        if (balance_stocks > 0):
            # the money lasted - save the final balance information
            final_balances.append(balance_stocks+balance_bonds)
percent_successful = (number_sims - ran_out)/number_sims * 100
final_balance_average = statistics.mean(final_balances)
final_balance_standard_deviation + statistics.stdev(final_balances)   
print("The money lasted", percent_successful, "percent of the time")
print("The average final balance when the money lasted was", final_balance_average)
print("The standard deviation in the final balance when the money lasted was", final_balance_standard_deviation) 

# output the simulation results
hist(final_balances, bins = 20)
show()  

# Our model was based on sampling from historical data.

######## DICE ROLLING ##################
#Imagine that you are rolling 3 dice and are interested in the sum of those dice.
#Use a Monte Carlo simulation to simulate 10,000 rolls of 3 dice. Use matplotlib
#to plot a histogram of the results.

import random
from matplotlib.pyplot import show, hist
rolls = []
for i in range(10000):
    roll = (random.randrange(6)+1) + (random.randrange(6)+1) + (random.randrange(6)+1)
    rolls.append(roll)
hist(rolls, bins=16)
show()  