# -*- coding: utf-8 -*-
"""
Created on Wed Jan 22 01:02:29 2020

@author: Richard
"""
y = 5
y += 1 #increases by

z = 8
z -= 1 #decreases by

m = 10
n = 10
m *= 2
n /= 2

balance = 1000.00
withdrawal_amount = 20.00
balance = balance - withdrawal_amount
#OR
balance -= withdrawal_amount

a = "John"
b = 3.14159
c =100
print(a)
print(a,b,c)
print("Hello", "\n", "World")
print("Hello"+"\n"+"World")
print(b+c)

X = input() #gets whatever user types in
Question3 = input("What is your favorite color?")
print(Question3)
b = input()
print("You entered", b)

a = input("Enter a value for a:") # input cusually comes in as a string
b = input("Enter a value for b:")
a = "1" # python can convert this to a float
b = "3.14159" #python cant convert this to an integer
# to convert string float to int, you either truncate or round
c = int(b)
d = round(b)

# to correct
a = int(input("Enter a value for a:")) # input is converted to an int
b = int(input("Enter a value for b:"))
name = input("What is your name?")
print("Hi,", name)
#OR
print("Hi, " + name)
#a variable is a box in short term memory
c =int(a)
d =float(b)