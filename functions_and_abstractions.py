# -*- coding: utf-8 -*-
"""
Created on Fri Jan 24 16:34:46 2020

@author: Richard
"""
# Functions and Abstractions
# Functions are groups of commands to get things done

# Functions demonstrate the most important idea in computer science, abstraction. Abstraction is simplifying of all the details away and viewing a complex system through a simpler interface. W abstract away the complexity. An example is memory, input/output, the programming language itself

# Other names for a function include routine, subroutine, procedure and method(used only in the context of OOP)
# A function always returns a value. Print() asks the computer to do something and is an example of calling the function, which is when a program asks a function to do what it was intended to do, like text getting printed

# Function calls are black boxes, we dont need to think about what it does.
# You get your input, do your thing, and output a value.

# withing a function you can make use of other function calls. This is a type of managing complexity. Programmers need to understand only the details of the level they are working on

def functionname(...): # header
    #details # body
    
def warn():
    print("Warning! Use program at your own risk.")
    a = 3
    print(a)
    warn()
    print("Welcome!")


############### THIS IS OUR WARN FUNCTION ###############################    
def warn():
    print("Warning! You are about to enter sensitive information.")
    ans = input("Do you wnat to continue? (Y/N)")
    if (ans == "n") or (ans == "N"):
        quit() # which is also defined
        
############### THIS IS OUR "MAIN" CODE ####################
warn()
name = input("Enter your name:")
warn()
address = input("Enter your address:")
warn()
ccn = ("Type in your credit card number:")
warn()
expiration = ("Enter the expiration date for your card:")

# When to use a function
# Any time you are doing the same task in different places in the code
# Any time you have a concept that you consider as a single unit

# Program that enables you manage your home business. Lets say you have a program that lets you enter clients,it does some other stuff to figure out how much the owe you, and then it prints a bill.

############### OUR PROGRAM #################
clientnames = []
clientaddresses = []
clientbalances = []
have_client = True
while (have_client):
    name = input("Enter the client's name:")
    address = input("Enter the client's address:")
    clientnames.append(name)
    clientaddresses.append(address)
    clientbalances.append(0)
    response = input("Is there another client (y/n)?")
    if response == "n":
        have_client = False
        
# More stuff here
for i in range(len(clientnames)):
    #Generate invoice
    if clientbalances[i] > 0:
        print("To: "+clientnames[i])
        print("At: "+clientaddress[i])
        print(clientnames[0]+", you owe a total of "+str(clientbalances[i]))
        print("Please pay your bill right away.")

# getting client information and printing invoice seem like a discrete idea so we generate a function
def getClient():
    name = input("Enter the client's name:")
    address = input("Enter the client's address:")
    clientnames.append(name)
    clientaddresses.append(address)
    clientbalances.append(0)
    return

def printInvoice():
    print("To: "+clientnames[i])
    print("At: "+clientaddress[i])
    print(clientnames[0]+", you owe a total of "+str(clientbalances[i]))
    print("Please pay your bill right away.")
    
########### USING OUR NOW DEFINED FUNCTIONS ##############
clientaddresses = []
clientbalances = []
have_client = True
while (have_client):
    getClient()
    response = input("Is there another client (y/n)?")
    if response == "n":
        have_client = False
        
# More stuff here
for i in range(len(clientnames)):
    #Generate invoice
    if clientbalances[i] > 0:
       printInvoice()

# The return statement allows us output a function
def getName():
    first = input("Enter your first name:")
    last = input("Enter your last name:")
    full_name = first + " " + last
    return full_name # variable is a tuple

name  = getName()


############## getGuess program ###################
# Write  routine that asks someone for their number guess and returns that value
def getGuess():
    number = input("What number do you guess? ")
    return number

guess = getGuess()

########## Using the return to output more than one value ########
# The return statement allows us output a function
def getName():
    first = input("Enter your first name:")
    last = input("Enter your last name:")
    full_name = first + " " + last
    return first, last

usefirst, userlast = getName() # a tuple actually gets returned here. remember that a tuple is a list of values separated by commas inside a parenthesis. here we assign the tuple

################ getBirthday function ################
# Write a function that asks a user to enter a birthday as a month, day, and year, and then returns that date
def getBirthday():
    month = input("Enter your birth month: ")
    day = input("Enter your birth day: ")
    year = input("Enter your birth year: ")
    return month, day, year

bday = getBirthday()
m, d, y = getBirthday()

####### PARAMETERS OF A FUNCTION ###############
# Parameters are the information inside parenthesis when calling a function.
# summing up positive integers
def getSum(x): # x is the parameter
    sum = 0;
    for i in range(x+1):
        sum += i
    return sum

total = getSum(100)

# OR
def getSum(x): # x is the parameter
   return x * (x+1) / 2

total = getSum(100)

# Function that computes factorials
def factorial(n):
    product = 1
    for i in range(1, n+1):
        product = product * i
    return product

print(factorial(20))

#### Passing more than one parameter  ###########
# finding sum in range of two values
def getRangeSum(a, b):
    sum = 0;
    for i in range(a, b+1):
        sum += i
    return sum

# OR
def getrangeSum(a, b):
    return b*(b+1)/2 - a*(a-1)
# OR
    def getrangeSum(a, b): # using our initial get sum function
        return getSum(b) - getSum(a-1) # here we compute the sum from one to the ending number and then subtracting the sum from one to just before the starting number. 

print("The sum from 1 to 100 is:", getRangeSum(1, 100)

# Some programs have a function hierarchy
# Becareful about abstractions
# occasionally functions have side effects, for exampls updating the original list
def sumList(mylist): # take list and sum up all the values in the list
    for i in range(1, len(mylist)):
        mylist[i] += mylist[i-1] # add previous value to current value
        return mylist[len(mylist)-1] # return final value in the list
testlist = [5, 3, 2, 1, 4]
print(sumList(testlist))
print(testlist) # list is different than before hand

# It's helpful to document functions. concisely saying what the function does. You do this using a docstring
def greet(name):
    """print a grreting.
    prints text "Hello, <name>" for parameter <name>."""
    print("Hello, "+name)
help(greet) $=# provides the information about the function and the doc string

#### METHODS ################
#There are also functions called methods that are part of objects. Foe example:
# variableName.methodFunction()
# file.close()