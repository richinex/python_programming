# -*- coding: utf-8 -*-
"""
Created on Sat Feb  1 21:02:06 2020

@author: User
"""
## Numpy vstack function ##################
y = np.array([1, 2, 3, 4, 5])
x1 = np.array([6, 7, 8, 9, 10])
x2 = np.array([11, 12, 13, 14, 15])
X = np.vstack([np.ones(5), x1, x2, x1*x2]).T
X 
## Chemical concentration in a reactor

from numpy import *
from scipy.integrate import quad, odeint
import matplotlib.pyplot as plt

# Data
t = arange(0, 4, 0.05)

# Define the ODE system
def deriv(state, t):
    C = state
    C_dot = 2 * t - 1/2 * (t**2)
    return  [C_dot]

soluc = odeint(deriv, [0], t)

C = soluc[:, 0]

import matplotlib . pyplot as plt
plt. close ('all ')
plt. plot(t, C)


########### FOURIER TRANSFORM ##################
#The integral known as the Fourier Transform deserves special attention
#for its applications in various branches of science. One of
#the most widespread uses is to decompose a time signal f(t) into
#its frequency components. The analysis of time signals and their
#decomposition into frequencies allows us to reveal hidden characteristics
#of the signals. Often, the time signal f(t) is a discrete sample, so the DFT is used.
# Humpback whale calling
from scipy.io import wavfile
import numpy as np
from matplotlib import pyplot as plt
import os
# Read the wav file
filein = 'data/mike4.wav'
samplerate , data = wavfile.read(filein)
N = len ( data )
# set a times axis , for plotting
times = np. arange (len(data))/float(samplerate)
dt= times [1] - times [0]
# Plot the wave
plt. close ('all')
plt. figure (1)
plt. plot (times , data)
plt. title ('Time Domain Signal')
plt. xlabel ('Time , s')
plt. ylabel ('Amplitude ( $Unit$ )')

## Set and apply a Hann window
# a Hanning window touches zero at both ends removing any discontinuity. The hanning window has a ratio of R/N of 1/4, hamming has 1/2
hann = np.hanning(len(data))
data_hann = hann*data # apply hann

# Perform FFT
YY = np.fft.fft( data_hann ) # Calc FFT
f = np.linspace(0, samplerate , N, endpoint = True )
# Normalize to /N
YYnorm = YY *2/N
# Discard 2nd half
fhalf = f[: int(N/2)]
YYnormhalf = YYnorm[:int(N/2)]
# Plot spectrum
plt. figure (2)
plt. plot (fhalf , abs (YYnormhalf))
plt. title ('Frequency Domain Signal')
plt. xlabel ('Frequency ( $Hz$ )')
plt. ylabel ('Amplitude ( $Unit$ )')
# Print some results
print ("\ nAnalysis of "+ filein )
print ('number of data N=',N)
print ('Sampling dt=',dt)
print ('Fmin Hz=',fhalf [0] , ' Fmax Hz=',fhalf[ -1])

# The peaks that emerge at show the calling (the sound) in frequency domain