# -*- coding: utf-8 -*-
"""
Created on Thu Jan 23 19:00:37 2020

@author: Richard
"""

# Files and strings
#Registers < Cache < Main Memory < Secondary Memory < Offline Memory
# Exceuting means bringing a file into main memoy and letting the processor execute all commands given

# Files are closely tie with strings.
# file format is one long string and file locations is given by strings

# Three basic things we need to do
# Open the file
# perform operations (read/write)
# close the file

myfile = open("Filename", "r") #myfile is the name to be used for our file inside the program
# filename is the name of the file in the computer e.g input.txt, ev.dat
# r tells the computer its an input operation,
# a means append to an existing file
# w means write to a file

infile = open("MyDataFile.txt", "r")
outfile = open("results.txt", "w")
# Do some stuffs
# close a file
myfile.close()


# Option . Automatically closes but only open for the section of file indented
with open("Filename", "w") as myfile:
    # Do something


infile = open("MyDataFile.txt", "r")
outfile = open("results.txt", "w")
# Do stuff
infile.close()
outfile.close()

# OR
with open("MyDataFile.txt", "r") as infile: # works best with one file
    with open("results.txt", "w") as outfile:
        #Do stuff here

# The write command 
# can only write one string at a time
# does not put in a new line character        
myfile = open("Filename", "w")
myfile.write("The number of countries is: " + str(196))
myfile.close()

# Suppose we have two variables volume1 and volume2 we wanna write
with open("results.txt", "w") as outfile:
    outfile.write("The first volume is " + str(hours_worked) + "\n")
    outfile.write("The first volume is " + str(num_remaining_payments) + "\n")
    
# Suppose we wanna read in file
myfile = open("Filename", "r") # read in variable
linefromfile = myfile.readline()
seconline = myfile.readline() # you can read as many files
outfile.write(linefromfile)
outfile.write(secondline)

outfile.close()
myfile.close()

# to read in allthe data
myfile = open("Filename", "r") # read in variable
linefromfile = myfile.readline() #we had to read a line first to get things to work
while linefromfile != "":
    #do something
    linefromfile = myfile.readline()
    
myfile.close()

# OR
myfile = open('Filename", "r")
for linefromfile in myfile: # a variable we wanna put in the data
    #do something
    
myfile.close()

# To get the entire content of the file as a string we use. File must be saved as a txt file.
myfile = open("Filename", "r")
linefromfile = myfile.read() #all datais connected into a single string
myfile.close()

# image files and sound files are binary. To read in we use
# write(wb)
myfile = open("filename", "wb")

# append (ab)
myfile = open("filename", "wb")

# read (rb)
myfile = open("filename", "rb")

# The path/folder gives the directory in which a file resides in

# Escape character \
#the next character should be interpreted differently than usual
print('"That\'s horrible," he said')
print("\"That\'s horrible,\" he said")
print('\"That\'s horrible,\" he said')

#\\ used for windows
# """ can be use as a multiline string besides the hashtag