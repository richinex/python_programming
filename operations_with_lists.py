# -*- coding: utf-8 -*-
"""
Created on Fri Jan 24 01:03:04 2020

@author: Richard
"""
# lists/array
# variables correspond to a box in memory.
daily_high_temps = [83, 80, 73, 75, 79, 83, 86]
# We add index to the location of the first element. The fisrst element will be right at the position stored. its offset by zero from the location stored.
print(daily_high_temps[0]) # the index could have an integer value

for i in daily_high_temps:
    print(i)

#OR

for i in range(7):
    print(daily_high_temps[i])

# the len() gives the length of a list
i = 0
while i < (len(daily_high_temps)):
    print(daily_high_temps[i])
    i = i + 1
    
# Summing using lists
days_in_month = [31, 28, 31, 30, 31, 30 ,31, 31, 30, 31, 30, 31]
num_days = 0
for i in range(12):
    num_days += days_in_month[i]
# OR
num_days = 0
for i in days_in_month:
    num_days += i

num_days = sum(days_in_month)

# we can merge list using addition
list1 = [3.1, 1.2, 5.9]
list2 = [3.0, 2.5]
list1 += list2

list1.append(3.9) # appends to the end of an existing list

# List of all the ages in the group
ages = [] # we start off with an empty list
age = int(input("Enter an age of a group member. Enter -1 when done"))
# operators "!=" means "does not equal"
while (age != -1):
    ages.append(age)
    age = int(input("Enter an age of a group member. Enter -1 when done"))
    
print(daily_high_temps[-1]) # gives the last item of the list. -3, third item from the end

# slicing
listvariable = [3.1, 1.2, 5.9, 4.6, 5.2, 1.4, 7.2]
listvariable[0:3] # first three elements
listvariable[4:6] # elements 4 and 5
listvariable[:6] # first six elements
listvariable[-3:] # last three elements
listvariable[:] # copy of list (all elements)
listvariable[1:6]  = [] # replaces with an empty space

days_in_summer = days_in_month[5:8]
days_at_beginning =days_in_month[:3]
days_at_end = days_in_month[-3:] # last three elements
days_at_extremes1 = days_at_beginning + days_at_end

# OR
days_at_extremes2 = days_in_month[:]
days_at_extremes2[3:9] = []
# Strings are a slight variation of a list

example = "Arthur, King of the Britons"
print(example[:6])
print(example[8:12])
print(example[-4:]) #last four characters

# However we can't delete parts of strings, or insert in the middle, as we did with regular lists

teststr = "Testing String Commands"
lowstring = teststr.lower()
capstring = teststr.capitalize()

print(lowstring)
print(capstring)

# Lists of lists
list_of_lists = [[1, 2, 3], [4, 5, 6], [7, 8, 9]]
print(list_of_lists[0]) # first list of list
print(list_of_lists[2][0]) # first element of second list

# You can have a heterogeneous list. but a better way is using an object.

# Tuple: variable, like a list, whose values can never change, and often will contain different types of data
credit_card = 'First National Bank', 1357256875236, 'January, 2023' # a tuple
car_tuple = "Volks", "century", 2007
make, model, year = car_tuple
print(make)
print(model) # better with tuples and not with list. we cant change the value of a tuple once its made

# code to compute the name with the maximum age in a list
names = []
ages = []
name = input("Enter a person's name or 'stop' to stop:" )
while (name != "stop"):
    age = int(input("What is the age of "+name+"? "))
    names.append(name)
    ages.append(age)
    name = input("Enter a person's name, or 'stop' to stop:")

maxindex = 0
for i in range(1, len(ages)):
    if ages[i] > ages[maxindex]:
        maxindex = i # we keep track of the index for the oldest age
        
print("The oldest person is", names[maxindex])