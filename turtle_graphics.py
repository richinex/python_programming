# -*- coding: utf-8 -*-
"""
Created on Mon Feb 10 23:43:04 2020

@author: Richard
"""
# A turtle will travel along a path that you define, leaving a pen mark behind it. As you control the turtle, the pattern that is left is revealed

# Pentagon
import turtle
turtle.shape("turtle")

for i in range(0,5):
    turtle.forward(100)
    turtle.right(72)
turtle.exitonclick()

# By combining these simple shapes and using nested loops (i.e. loops inside other loops) it is possible to create beautiful patterns very easily
import turtle
for i in range(0,10):
    turtle.right(36)
    for i in range(0,5):
        turtle.forward(100)
        turtle.right(72)
turtle.exitonclick()

import turtle
turtle.left(90)
turtle.forward(100)
turtle.right(90)
turtle.forward(50)
turtle.right(90)
turtle.forward(50)
turtle.right(90)
turtle.forward(50)
turtle.exitonclick()

import turtle
turtle.left(90)
turtle.forward(100)
turtle.right(90)
turtle.forward(50)
turtle.right(90)
turtle.forward(50)
turtle.right(90)
turtle.forward(50)
turtle.penup()
turtle.left(180)
turtle.forward(50)
turtle.pendown()
turtle.right(90)
turtle.forward(50)
turtle.exitonclick()