# -*- coding: utf-8 -*-
"""
Created on Wed Jan 29 22:13:02 2020

@author: Richard Chukwu
"""

## GRAPHS AND TREES ######
## The things we are representing are the node or vertex (that is the cities). The connection between nodes are edges (the roads)
# In terms of writing code, graphs can have more than one representation. one that is more global and one that focuses on adjacent neighbors.

## The more global option is to represent a graph as two lists:
    # A list of nodes
    # A list of edges
    
## Each node contains information about itself for example each of our city nodes might contain:
# Node:
    # City name
    # Other information, such as population, GPS coordinates, ets
# Edge:
    # Name of city 1
    # name of city 2  
    # If it is a weighted graph, the edge will also store a weight which in this case might be the length of the road in miles or km
    
## Let us write some code for nodes and edges:
# We have two classes: Node class and edge class
    # remember that a class helps us encapsulate the stuff that goes together.
# The node class should incorporate all the stuffs in a node and the edge class should incorporate all the stuff in an edge   
    
class node:
    def __init__(self, name, population = 0):#init set the instance attributes. Each node can have a diff node and population
        self._name = name
        self._pop = population
    def getName(self):
        return self._name
    def getPopulation(self):
        return self._pop
        
class edge:
    def __init__(self, name1, name2, weight = 0):
        self._city1 = name1
        self._city2 = name2
        self._distance = weight
        
    def getName1(self):# accessory functions
        return self._city1
    def getName2(self):
        return self._city2
    def getNames(self):
            return (self._city1, self._city2)
    def getWeight(self):
            return self._distance

cities = []
roads = []
    
city = node('Rivertown', 100)
cities.append(city) # add to the city list
city = node('Brookside', 1500)
cities.append(city)
city = node('Hillsview', 500)
cities.append(city)
city = node('Forrest City', 800)
cities.append(city)
city = node('Lakeside', 1100)
cities.append(city)
road = edge('Rivertown', 'Brookside', 100)
roads.append(road)
road = edge('Rivertown', 'Hillsview', 50)
roads.append(road)
road = edge('Hillsview', 'Brookside', 130)
roads.append(road)
road = edge('Hillsview', 'Forrest City', 40)
roads.append(road)
road = edge('Forrest City', 'Lakeside', 80)
roads.append(road)

print(road.getName1())

### Using Edges ###
## How might you use this to?
    # Find the total population that lives on a road? Yow'd need to find the population of cities at each end which is stored in the city nodes. The road only knows the name of the cities
    # Write a function to get the combined population along edge 0
road = roads[0]
pop1 = 0
pop2 = 0
for city in cities:# loop through the city list
    if city.getName() == road.getName1():
        pop1 = city.getPopulation()
    if city.getName() == road.getName2():
        pop2 = city.getPopulation()

total_population = pop1 + pop2

## Instead of using the names of the two cities, we could use the index of the two cities in the list of nodes. The index is just a where they are in that list acording to the order. We remodify the edge method
class node:
    def __init__(self, name, population = 0):#init set the instance attributes. Each node can have a diff node and population
        self._name = name
        self._pop = population
    def getName(self):
        return self._name
    def getPopulation(self):
        return self._pop
        
class edge:
    def __init__(self, index1, index2, weight = 0):
        self._city1 = index1
        self._city2 = index2
        self._distance = weight
        
    def getIndex1(self):# accessory functions
        return self._city1
    def getIndex2(self):
        return self._city2
    def getIndices(self):
            return (self._index1, self._index2)
    def getWeight(self):
            return self._distance

cities = []
roads = []
    
city = node('Rivertown', 100)
cities.append(city) # add to the city list
city = node('Brookside', 1500)
cities.append(city)
city = node('Hillsview', 500)
cities.append(city)
city = node('Forrest City', 800)
cities.append(city)
city = node('Lakeside', 1100)
cities.append(city)

road = edge(0, 1, 100)
roads.append(road)
road = edge(0, 2, 50)
roads.append(road)
road = edge(2, 1, 130)
roads.append(road)
road = edge(2, 3, 40)
roads.append(road)
road = edge(3, 4, 80)
roads.append(road)

road = roads[0]

## Constant time operation. In using an index, the list of cities must remain unchanged
pop1 = cities[road.getIndex1()].getPopulation()
pop2 = cities[road.getIndex2()].getPopulation()

total_population = pop1 + pop2

## The above is an example of indirection. This is using an index to refer to an item in a list instead of the item's name or value. A dictionary is another way of getting this effect.

## Indirection works well for the second way we could store graphs called adjacency list. Let's say that we want to be able to take a given city and find all the roads from that city. This is a painful operation using the above approach. Instead of keeping a single list of all edges in the graph, lets instead keep a local list of the edges of each node.
# Each node will keep track of the edges connected to it. in this case we will have just one list

## Local list of edges
# list of nodes
# Nodes
# City name
# other information, such as population, GPS coordinates, etc
# list of edges with:
    # keeps track of city at the other end
    # weight,e.g length of the road or time to travel the road
class node:
    def __init__(self, name, population = 0):#init set the instance attributes. Each node can have a diff node and population
        self._name = name
        self._pop = population
        self._roads = [] # each element here is an edge
    def getName(self):
        return self._name
    def getPopulation(self):
        return self._pop
    def getRoads(self):
        return self._roads
    def addNeighborRoad(self, e): #takes n existing road as input and appends and adds it onto the list of roads connected to the city stored in the roads list in the class
        self._roads.append(e)
    def addNeighbor(self, city, weight = 0):# takes in a city name and a weight for the edge as input parameters. it first creates a road to that city with that weight and then it appends that road onto the roads list
        e = edge(city, weight)
        self._roads.append(e)
        
class edge:
    def __init__(self, city, weight = 0): # we could have used index also
        self._city = city
        self._weight = weight
    def getCity(self):
        return self._city
    def getWeight(self):
        return self._weight
    
# We can create a set of cities just like before
cities = []
    
city = node('Rivertown', 1000)
cities.append(city) # add to the city list
city = node('Brookside', 1500)
cities.append(city)
city = node('Hillsview', 500)
cities.append(city)
city = node('Forrest City', 800)
cities.append(city)
city = node('Lakeside', 1100)
cities.append(city)

## However creating an edge in the graph  thats a connection between two cities is more complicated. To make things easier we define two functions
def addRoad(name1, name2, weight = 0):
    # creates a road list for the cities by creating new edge objects. one for easch city
    e1 = edge(name2, weight)
    e2 = edge(name1, weight)
    for city in cities: #we search thru to find the matchineg city name, and when it finds it add teh edge to that cith by calling the addNeighborRaod. In this way, both cities get a local edge saying they are connected to a neighbor.
        if city.getName() == name1:
            city.addNeighborRoad(e1)
        if city.getName() == name2:
            city.addNeighborRoad(e2)

# alternatively,            
def addRoad2(name1, name2, weight = 0):
    for city in cities:
        if city.getName() == name1:
            city.addNeighbor(name2, weight)
        if city.getName() == name2:
            city.addNeighbor(name1, weight)

addRoad('Rivertown', 'Brookside', 100)
addRoad('Rivertown','Hillsview', 50)
addRoad('Brookside', 'Hillsview', 130)
addRoad('Hillsview', 'Forrest City', 40)
addRoad('Forrest City', 'Lakeside', 80)

for city in cities:
    print('Neighbors of '+city.getName())
    roads = city.getRoads()
    for road in roads:
        print('     ', road.getCity(), 'at a distance', road.getWeight())
        
## If we wanted to find every edge, this data structire isnt as well suited as the previous one.
        # Summary of the ways of storing graphs
        # Global list (most useful for looking at all the edges like 3D graphics)
        # Adjacency list(where you keep a list of the edges within each node) is useful in most of our typical graph connections ei. social networks, airline connections and so on. The adjacency list works well because most graph algorithms are already designed to work just by looking at one node at a time and its neighbors.
        # The third way is an adjacency matrix, instead of a list their are matrix entries that not which nodes are connected. this can be useful for operations where you need to quickly run over lots of different values or perform certain computations that can be  expressed using linear algebra. Used when there are a lot of edges, a key factor when graphs are use. It is also the fastest of the representations when you need to check whether a pair of cities is connected.
# For any given graph, we can define a variety of graph algorithms. The representation used to store the graph determines how the algorithm will perform. Graph algorithms let us analyze all sorts of stuff about the structure of graphs
##For example the breadth first search algorithm lets us analyze how many degrees of separation there really are between two people in a social network
# with roads, we can usually travel in either direction. We just say those nodes are connected. Graphs like this are called undirected graphs. A graphs where people are friends is undirected. We can also have cases where two things are linked but the connection is not equal. for example cities connected by airline routes. Not all air line routes fly round trips. sometimes a plane goes through cities. This gives rise to directed edges, and the resulting graph made up of directed edges is called a directed graph or sometimes a digrapgh. Web pages and the links between them form a directed graph. If web page a has a link to web page b, there is not necessarily one from b back to a. sdtoring a directed graph is not much different from storing a undirected graph. If we have a big list of edges, then we just need to make sure that each edge has a start and an end.
# For a global list
class node:
    def __init__(self, name, population = 0):#init set the instance attributes. Each node can have a diff node and population
        self._name = name
        self._pop = population
    def getName(self):
        return self._name
    def getPopulation(self):
        return self._pop
        
class edge:
    def __init__(self, name1, name2, weight = 0):
        self._city1 = name1
        self._city2 = name2
        self._distance = weight
        
    def getName1(self):# accessory functions
        return self._city1
    def getName2(self):
        return self._city2
    def getNames(self):
            return (self._city1, self._city2)
    def getWeight(self):
            return self._distance
# For an adjacency list. Each node in our previous example is essentially a directed edge because it is stored with the starting point.
### Connected Graphs 
## We say a graph is connected idf there is some sequence of edges connecting every pair or nodes  .
## We say a graph has a cycle is it has a way to follow a set of edges and end up back at the start

## Directed Graph in an application involving money 
# Different currencies can be traded from one currency to another at some exchange rate. For any pair of currencies, theres some rate which you can trade one currency into the other. We can form a directed graph of these exchanges. It will be a really dense graph because every currency can be exchanged for a different one. It is a weighted graph also because each rate can give the rate of exchange for the first one into the second one.
# Now currency arbitrage takes advantage of an imbalance in the currency exchange rates, so that you can make money through changing currencies. This is how this would work:
# lets say:
    # hypothetical exchange rate
    # 0.95 euros per USD
    # 0.75 pounds per Euro
    # 1 pound for 175 Yen
    # 1 Yen for 0.0085 USD
    
# So if you started with 1000 USD
    # 1000 USD = 950 Euros
    # 950 Euros = 712.50 Pounds
    # 712.50 Pounds = 124,687 Yen
    # 124,687 Yen = 1059.84 USD # more than you started with.
# if we store currency exchange rates as a directed graph, what we want to find out a cycle in which the product  of all the edges is grater than 1 and this means we could use arbitrage to generate money.
## First we create classes
class Currency: # currency class
    def __init__(self, name):#init () sets up the name and an empty list of exchange rates both as instance attributes
        self._name = name
        self._rates = [] # each element here is an edge
    def getName(self): # accessory() to return name
        return self._name
    def getRates(self): # accessory() to return exchange rate
        return self._rates
    def addRateEdge(self, e): # assumes we have already created an edge that describes the exchange rate
        self._rates.append(e)
    def addRate(self, currency, weight = 0):# takes in the currency name and the exchange rate for that currency and it creates the edge
        e = Edge(currency, weight)
        self._rates.append(e)
    def findArbitrage(self):# call for a particular currency
        for rate in self._rates: # for loop for all the exchange rates in that currency
            for c in currencies:# for each currency, we have another loop to find the particular currency in the overall list
               if c.getName() == rate.getCurrency():
                   if c.findRates(self._name, rate.getExchange(), 1):# call find rates for that currency
                       return True
                   return False # no cycle in exchange
    def findRates(self, maincurrency, current_value, depth):
        if self._name == maincurrency:# first base case
        #We are back at the main currency - see if the value > 1
            if current_value > 1:
            # We found a way to make money!
                print("We found a way")
                print(current_value, self._name)# show what combined exchange rate is
                return True
        if depth > 3:
            # 2nd base case for recursion: too many exchanges
            return False # quit
        for rate in self._rates:
            conversion = current_value * rate.getExchange()
            for c in currencies: # find currency that conversion rate corresponds to
                if c.getName() == rate.getCurrency(): # once it finds that, it recursively calls itself within the if statement. the call to itself  passes in the main currency , the new combined exchange rate, and increments the number of exchanges so far by 1. If that returns true, it means there was acycle to this point and it prints out the combined exchange rate, it currency name and returns true
                    if c.findRates(maincurrency, conversion, depth+1):
                        print("from", current_value, self._name)
                        return True
        return False
        
# First we create a member function of the currency class called findarbitrage

        
class Edge:
    def __init__(self, currency, rate = 0):
        self._currency = currency
        self._exchange = rate
        
    def getCurrency(self):# accessory functions
        return self._currency
    def getExchange(self):
        return self._exchange
    def setExchange(self, exchange):
            self._exchange = exchange
            
def addExchange(name1, name2, exchange1):
    # creates a road list for the cities by creating new edge objects. one for easch city
    e1 = Edge(name2, exchange1)
    e2 = Edge(name1, 1.0/exchange1)
    for currency in currencies: 
        if currency.getName() == name1:
            currency.addRateEdge(e1)
        if currency.getName() == name2:
            currency.addRateEdge(e2)

# alternatively,            
def addExchange2(name1, name2, weight = 0):
    for currency in currencies:
        if currency.getName() == name1:
            currency.addRate(name2, weight)
        if currency.getName() == name2:
            currency.addRate(name1, 1.0/weight)

currencies = []
    
currency = Currency('Dollar')
currencies.append(currency) # add to the city list
currency = Currency('Pound')
currencies.append(currency)
currency = Currency('Euro')
currencies.append(currency)
currency = Currency('Yen')
currencies.append(currency)

# Set up exchange rates between the pairs
addExchange('Dollar', 'Pound', 0.7)
addExchange('Dollar', 'Euro', 0.95)  
addExchange('Yen', 'Dollar', 0.0085) 
addExchange('Pound', 'Euro', 0.75)
addExchange('Pound', 'Yen', 175) 
addExchange('Euro', 'Yen', 233.3)         
## Functions to let us specify pairs of currencies

## Now we wanna find out if there is some conversion path that will let us convert between these to make money
## We define our main routine

                    
## Using the findArbitrage on the first currency
currencies[0]. findArbitrage()

#We found a way
#1.0411012499999999 Dollar # the back to USD
#from 122.48249999999999 Yen # then to yen
#from 0.5249999999999999 Euro # the to euros
#from 0.7 Pound # we convert 1 dollar to 0.7 pounds

#################  TREES ###################
## A connected, undirected graph that does not have a cycle is called a tree.