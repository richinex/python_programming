# -*- coding: utf-8 -*-
"""
Created on Fri Jan 24 22:48:58 2020

@author: Richard
"""
###### PARAMETER PASSINGM SCOPE AND MUTABLE DATA ###############
# Parameters are the main way to get information into the function
# Specified inside parentheses
# Values passed in to the function call
# Seen within the function as variables

## TOPICS ##
# When a parameter or variable is in scope
# How to work with data types that are mutable
# What it means for parameters to have default values


## In scope: A point in a program when a variable is defined and useable. Scope helps us keep straight what we are referring to when we use a name. Scope helps us keep stuff where it belongs

# Function that returns the maximum of two values
def maxdemo(val1, val2): # header
    if (val1 > val2): # body
        return val1
    else:
        return val2

a = 1
b = 2
c = maxdemo(a,b) # main program: stuff that gets executed. Computer skips the function definition and executes the main program. The function's area of memory is called the function activation record( temporary record), and gets destroyed when the function gets executed

# Function that copute tests scores
def testscore(numcorrect, total):
    numcorrect += 5; # computer replaces the val for numcorrect in  the FAR
    temp_value = numcorrect / total
    return (temp_value * 100)

a = 12
b = 20
c = testscore(a,b)

# The function parameters numcorrect and total have a scope of just the function itself
# The variable temp_value has a scope from the point it is defined till the end of the function. These parameters are out of scope in the main program.

# Another example
def testscore(numcorrect, total):
    numcorrect += 5; # computer replaces the val for numcorrect in  the FAR
    a = numcorrect / total
    return (a * 100)

a = 12
b = 20
c = testscore(a,b)

# Another example: Sourcing from the local variable/main routine. Not a good way to write code. Conceptual complexity has not been reduced since we have something not contained in our parameter list that affects our function. The right thing to do is to ad a as a parameter
def testscore(numcorrect, total):
    numcorrect += a; # computer replaces the val for numcorrect in  the FAR
    temp_value = numcorrect / total
    return (temp_value * 100)

a = 12
b = 20
c = testscore(a,b)


#### USING GLOBAL VARIABLES #####
# A global variable defined in the function is the exact same variable as in the main program.
# Useful when we want to initialize values or,
# Read in data and set up variables
def initialize():
    global fuellevel # value of fuellevel is changed in the program
    fuellevel = 100.0
    
fuellevel = 0
initialize()
print(fuellevel)

# Pass by value (python): Transmitting the value of a parameter when a function call is made
# Pass by reference (C++ or using mutable variables): Local parameter is exactly the same as the calling parameter, sharing the same memory location. So if we change the local parameter in that case, we get a different effect in the global parameter

## Mutable variables bring us to the surprising fact that there are times that we can modify the value on the calling side through a function. Variables can be either miutable or immutable.
# Mutable: Variable is changeable when passed as a parameter.
# Immutable: Variable cant be changed when passed as a parameter.
# most of our variable types are immutable: int, float and string, i.e when we pass it as a function parameter, the original value cant change, we just copy the value into the new memory location that is part of the function activation record. The immutable value staus the same on the calling side.

# Mutable data types include lists,objects, and so when we pass a list as a parameter the value in the list can actually change
# For example:
def adder(val1, val2):
    val1 += val2
    
num1 = 3
num2 = 4
adder(num1, num2)
print(num1) # nothing changes in the local parameters(calling side variables)

# However when we use a list,
def adder(val1, val2):
    val1 += val2
    
num1 = [1, 2]
num2 = [3, 4]
adder(num1, num2)
print(num1)

# What if we pass a scope list variable within the function
def adder(val1, val2):
    val1 = [5]
    
num1 = [1, 2]
num2 = [3, 4]
adder(num1, num2)
print(num1) # No change why?
# We need to understand that a mutable data type is a reference (pointer) to data values. This reference is automatically being de-referenced, so it seems as though you are just dealing with the data it refers to.

# We're always passing-by-value into a function: We're always making a copy of the value, but sometimes a copy of the value is a reference to something else in memory that can be changed.

# An example to test understanding
def DoSomething(in1, in2):
    in1 += [7]
    in2 = [8]
    
a = [1, 2, 3]
b = [4, 5, 6]
DoSomething(a, b)
print(a) # gets changed by the function since the function modifies what was in the list
print(b) # not changed since the function tries to assign a completely new value to it

# Default parameters
# We give a default val;ue to a parameter that can be used if the user does not define it. Foe example:
def calc_miles(gallons, mpg =20.0):
    return gallons*mpg

print(calc_miles(10.0, 15.0))
print(calc_miles(10.0))

### MADLIB #####
def madlib(animal = "Dinosaur", adjective = "Green", city = "Chicago", food = "Spaghetti"):
    return "You'll never believe what I saw in " + city, ". It was a " +adjective+ " " +animal+ "! And, it was eating "+food+"!"

print(madlib())

# Parameters are mapped in order from left to right. When you specify parameters by listing the default local parameter and an equal sign, it is called a keyword argument. for this kind of parameter, you specify by position first, and the give any keyword arguments after it. For example:
print(madlib("cat", food = "pizza", city = "Denver"))

# Use only default values only if the default will be an immutable value