# -*- coding: utf-8 -*-
"""
Created on Sat Jan 25 11:45:55 2020

@author: User
"""

def SumofSquares(n):
    sum = 0
    for i in range(1, n+1): # n+1 because range does not include the last number.
        sum += i*i
    return sum