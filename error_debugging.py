# -*- coding: utf-8 -*-
"""
Created on Sat Jan 25 00:31:07 2020

@author: Richard
"""

####### ERROR TYPES, SYSTEMATIC DEBUGGING AND EXCEPTIONS ###########
# Nemesis of all computer programmers.

# Syntax errors: spelling errors. found by compiler.
# Runtime errors: Often occur when input is incompatible with what's required
# Logic errors: Program runs just fine, but with incorrect output.(operator, variable definition , indentation). logic errors are the most difficult to track down because they are in your thinking, and not in your code.

# Three stages to debugging
# Create thorough tests
# isolate the condition that causes a bug to occur
# correct the error
# test the fix
# It's good to build a test suite: A set of tests that we will run against our code to make sure its working right. Grow test suite as code grows. Check edge cases and Unique cases.

# Insert print statements
# Analyze historical data
minsofar = 120 # set to extreme high value
maxsofar = -100 # set to extreme low value
numgooddates = 0
sumofmin = 0
sumofmax = 0
raindays = 0
for singleday in gooddata: # loop through data and update values
    numgooddates =+ 1
    sumofmin += singleday[1]
    sumofmax += singleday[2]
    if singleday[1] < minsofar:
        minsofar = singleday[1]
    if singleday[2] > maxsofar:
        maxsofar = singleday[2]
    if singleday[3] > 0:
        raindays += 1
print(sumofmin)
print(numgooddates)        
avglow = sumofmin / numgooddates # calculate avg min
avghigh =sumofmax / numgooddates # calculate avg max
rainpercent = raindays / numgooddates * 100

### Use a debugger
# Tool that helps a programmer examine the code in detail so a bug can be isolated
# Breakpoint: A way of telling the computer to run the code up to this point, but then stop, or break
# In pycharm, the frames window shows the sequence of functions that have been called. It also shows how deep for nested function calls.
# The watches window allows you set variables to be watched
# The main difference between logic and runtime errors is the way we treat these errors. Python deals with runtime errors through exceptions.
# Exceptions are ways of handling the special error conditions that can occur when a program is running.
# We handle this trhough try except blocks

try:
    #commands to try out. codes you wanna check for exception
except <name of exception>: #identifying which error you wanna deal with
    #how to handle error. # code to run if you encounter the error
    
# For example
filename = input("Enter name of file:")
try:
    myfile = open(filename, "r")
except OSError: #catches the exception
    print("That file could not be opened. Using default file.")
    myfile = open("ttc_weather.csv", "r")
# list of errors: 
# BaseException
# ZeroDivisionError
# AttributeError
# FloatingPointError
# ImportError
# Memory Error
# NameError
# TypeError
# SyntaxError
# SystemError
# OSError

# Using Multiple Except Blocks
# For example
def computeDivision(first, second):
    try:
        division = first/second
    except TypeError:
        print("You didn't enter two floating point numbers!")
    except ZeroDivisionError:
        print("Don't give a zero for the second number!")
    except:
        print("There was some other exception.")
    return division

# We could also add some else blocks
def computeDivision(first, second):
    try:
        division = first/second
    except TypeError:
        print("You didn't enter two floating point numbers!")
    except ZeroDivisionError:
        print("Don't give a zero for the second number!")
    except:
        print("There was some other exception.")
    else:
        print("Great - no exceptions were raised!")
    return division
        
# We could also add the finally block that comes at the very end. It gets executed after everything else whether there were exceptions or not
def computeDivision(first, second):
    try:
        division = first/second
    except TypeError:
        print("You didn't enter two floating point numbers!")
    except ZeroDivisionError:
        print("Don't give a zero for the second number!")
    except:
        print("There was some other exception.")
    else:
        print("Great - no exceptions were raised!")
    finally:
        print("The function is ending")
    return division
     
# We can raise exceptions ourselves. We use the word raise.
def computedate(month, day, year):
    try:
        if(month <= 0) or (month> 12)
        raise TypeError
    except TypeError:
        print("Invalid date: Please reenter:")
        day = int(input("Enter the day: "))
        month = int(input("Enter the month: "))
        year = int(input("Enter the year: "))
        
# Function to find a pattern of length 5 in a sequence
def find_pattern(pattern, sequence):
    if len(pattern) != 5
    #expected pattern of length 5
    raise TypeError
    
# be careful whe using exceptions. Use only for exceptional exceptions. When problems cant be detected.
day = int(input("Enter the day: "))
month = int(input("Enter the month: "))
year = int(input("Enter the year: "))

while (month <= 0) or (month> 12):
   print("Invalid date: Please reenter:")
        day = int(input("Enter the day: "))
        month = int(input("Enter the month: "))
        year = int(input("Enter the year: ")) 

******************OR**************************
try:
    if(month <= 0) or (month> 12)
        raise TypeError
except TypeError:
    print("Invalid date: Please reenter:")
    day = int(input("Enter the day: "))
    month = int(input("Enter the month: "))
    year = int(input("Enter the year: "))
        