# -*- coding: utf-8 -*-
"""
Created on Wed Jan 29 21:27:24 2020

@author: UserRichard Chukwu
"""
#### RECURSION AND RUNNING TIMES ###############
# The choice of which algorithm to choose can be critical. So how do we know wheter a particular algorithm is a good one to use in our program. The two broad approaches to explore are Iteration and Recursion.

## Recursion: One of the most interesting ideas in comp sci. We will apply recursion to:
# Form sorting algorithms
    # First we set up programs using pseudocode
    # Use algorithm analysis to compare the algorithms and see which routine is the best.
    
## The great trick in recursion is that a function is calling itself. An example is pip which stands for pip installs python or pip installs packages.
#def countdown(n):
#    while n > 0:
#        print(n)
#        n -= 1
#    
#countdown(5)

def countdown(n):  
    print(n) # print the number n
    if n > 0:
        countdown(n-1) # print the numbers from n-1 to 0
countdown(5)

# Iteration is better than recursion for counting down frm n to zero. But for stuffs that dont have a non recursive solution we rely on recursion.
# One of the main problem solving approaches that rely on recursion is divide an conquer.The idea is that it is easier to deal with two smaller problems than one big one.
# In computing when we use the term Divide and conquer, we are taking a large data set and dividing it into subsets that we handle independently. two algorithms that rely on divide and conquer are:
## Merge sort:
# Assume we have  a list of unordered numbers.
unordered_list = [3, 9, 8, 1, 5, 2, 12, 7]
# first we divide in two:
unordered_list_1 = [3, 9, 8, 1] # and
unordered_list_2 = [5, 2, 12, 7]
# second step is to sort each of those list.
# the sorting process is an example of divide and conquer. We take one large sorting proble and reducing it to two smaller sorting problems and solve each of those recursively.
# Finally there is a merge stage, where we merge each of the sorted lists into one bigger sorted list. To merge, we walk through both lists, pulling out the smallerst one left from which ever list

## Input: unsorted list L, length n
## Output: L, with elements sorted smallest to largest

if n <= 1: # check to see if we have a list of length 1. if true return L
    return L # base case: The point at which the recursive routine stops.
#L1 = L(0:n/2-1) L2 = (n/2:n-1)
#MergeSort(L1) MergeSort (L2)
# L = Merge(L1, L2) # Merge will be defined separately
    
def mergeSort(L):
    n = len(L)
    if n <= 1: # base case
        return
    L1 = L[:n//2] # form two shorter list using an integer dvision
    L2 = L[n//2:]
    mergeSort(L1)
    mergeSort(L2)
    merge(L, L1, L2)
    return

def merge(L, L1, L2):
    i = 0 # keeps track of L
    j = 0 # keep track of L1
    k = 0 # keep track of L2
    # now we go through the L1 and L2 arrays pulling the smallest value between them and putting it into the L array 
    while (j < len(L1)) or (k < len(L2)):
        if j < len(L1):#first check to see if we are at the end of the L1 array
            if k < len(L2):
                #we are not at the end of L1 or L2, so pull the smaller value
                if L1[j] < L2[k]:
                    L[i] = L1[j]
                    j += 1
                else:
                    L[i] = L2[k]
                    k += 1
            else:
                #we are at the end of L2, so just pull from L1
                L[i] = L1[j]
                j += 1
        else:
            #we are at the end of L1, so just pull from L2
            L[i] = L2[k]
            k += 1
        i += 1
    return  

favorite_foods = ['pizza', 'barbeque', 'gumbo', 'chicken and dumplings', 'pecan pie', 'ice cream']
mergeSort(favorite_foods)

## The next routine is quicksort., called so because it work pretty fast on typical cases. The idea here is still divide and conquer but the division is different from that of merge sort
# In quicksort, we pick some value to split everything around. Typically the first value in the list. This is called the pivot. We then form two new lists. One with all the value less than  the pivot and one with all the values more than the pivot.next we sort each of those lists again with a recursive call to quick sort. After that the whole list is sorted.

## Input: unsorted list L, length n
## Output: L, with elements sorted smallest to largest

#1.if n <= 1: # check to see if we have a list of length 1. if true return L
#    return L # base case: The point at which the recursive routine stops.
#2. Pick the pivot
    #pivot = L[0]
#    Use remaining elements to form list L1 less than, and L2 greater than the pivot
# quicksort(l1) quicksort(l2)
# L = join(l1, pivot, L2)

def quickSort(L):
    n = len(L)
    if n <= 1: # base case
        return
    # pick pivot
    pivot = L[0]
    # form lists less/greater than pivot
    L1 = []
    L2 = []
    for i in range(1, n): #because we set pivot to L[0], we start from1 or else we use for element in list
        if L[i] < pivot:
            L1.append(L[i])
        else:
            L2.append(L[i])
    #sort sublists
    quickSort(L1)
    quickSort(L2)
    # join the sublists and pivot
    L[:] = []
    for element in L1:
        L.append(element)
    L.append(pivot)
    for element in L2:
        L.append(element)
    return
favorite_foods = ['pizza', 'barbeque', 'gumbo', 'chicken and dumplings', 'pecan pie', 'ice cream']
quickSort(favorite_foods)

## Summary of Sort routines:
# Selection sort: We kept selecting the next smallest value
## Insertion sort: where we take the next value and insert it into the sorted list
## Merge sort: We divide the list into two equal parts and sorted those recursively
# Quicksort: Where we divided the whole list into smaller and larger parts around the pivot and soort that recursively.

## Choosing Between algorithms
# Easiest for programmer to understand
# Easiest to write code for
# The best choice should be free of programmer bias.
# To access efficieny, we use Asymptotic analysis. this looks at how a function performs as the input size grows larger and larger.
# We ask, how does the running time increase as the input increases.The input size is the size of the list.
# In essence we ask, what will happen if I double the input size.
# What part of the running time do we care about? worst case, average case or best case.

## If I have a list with n items in it and we do a standard linear search.
# best case: our item is the first item in the list
# worst case: we go through all n items and its not in the list or its the last.
# on average we go through half the items (n/2)
# now what happens if we have a list twice the size
## best case: our item is the first item in the list
# worst case: we go through all 2n items and its not in the list or its the last.
# on average we go through half the items (2n/2=2).
# Notice that when we double the input size, we double the worst and average case times. We define this as a linear process since the running time is a linear function of the input size O(n). Big O notation

##In binary search where we divide the list each time
    #Best case: first item in the list
    # worst case: we keep dividing by 2 until down to a single element. For a list less than len(n), we have log2(n) checks required (2_n+1 through 2_n -1)
    #in the average case we have log2(n) - 1
    # If we souble we have log2(n) + 1
    # We describe this as a log function since the running time is a logarithmic function of the input size and we write this as O(log2n)

# Clearly if we are choosing between a linear process and a logarithmic process, given a sufficiently large dataset, always prefer the log process
    
## For our sorts:
# Quicksort can be implemented efficiently so it is often faster than merge sort in practice.  
### PYTHON'S SORTING ALGORITHM ##
    # Designed to be more efficient than selection sort, insertion sort, merge sort and quick sort. Initially used quick sort. insertion works faster on smaller problems
    # uses merge sort for the over all problem
    # switches to insertion sort if problem is small
    # checks to see if portions of the list are sorted already
    # It uses recuresion when there is a merge soort and avoids it otherwise. why? Well recursion is useful way of describing certain calculations and the only good way for some. sometimes avoid recursion
    
####
    ## Recursively
def Fib(n):
    if n == 0:
        return 0
    elif n == 1:
        return 1
    else:
        print(Fib(n-2))
        print(Fib(n-1))
        return Fib(n-2) + Fib(n-1)
Fib(100)

# Each call generates twice the number of function calls generates twice the number of function calls as the previous level. eig fib 100 generates 2^ 100 function calls before it reaches base cases. nonillion function calls. the algotithm takes exponential time.

# however we didnt need recursion to compute the fibonacci series
def Fib2(n):
    F = [0, 1]
    for i in range(2, n+1):
        #compute F[i]
        F.append(F[-2] + F[-1])
    return F[n]
print(Fib2(100))
# linear run time instead of the exponential recursive one