# -*- coding: utf-8 -*-
"""
Created on Fri Jan 24 11:13:47 2020

@author: Richard
"""
# Top Down Approach
# Take a problem and break it down into bite sized pieces
# begin by looking at the big picture and work your way down from the top
# If you continously analyze a problem and break it down into smaller conceptual ideas, eventually you'll reach the point where the idea you need to express can be written in just a single line of code.

# The split command
phonenum = "888-345-9876"
areacode, prefix, suffix = phonenum.split(',')
print('('+areacode+')'+prefix+'-'+suffix)

# If you ignore the separator argument, whitespace is used as the separator
sentence = "Look, it's a bird! \n Very interesting"
words = sentence.split()
print(words)
# Top-Down Design Example:

# We have a file containing temperatures in a city over time. e.g a csv file
# Analyze the file to understand weather patterns on a particular day or week
# Answer queries about past temperatures on a calendar day
# Hottest/coldest temperatures
# Average daily high/low
# Largest one-day temperature swing
# High/low temperature in a month

# Broadest level: Read in data, analyze data and present data. 

####### Read in Data: Open file, read in lines of file and close file
## Open File
filename = input("Enter the name of the data file: ") # as stored in the PC
infile = open(filename, "r")
#print(infile.read()) # this code helps us visualise the data
# Test code

# Read lines from file: loop over all lines
datalist = []

for line in infile:
    # get data from line
    date, h, l, r = (line.split(',')) # this returns strings
    lowtemp = int(l)
    hightemp = int(h)
    rainfall = float(r)
    m, d, y = date.split('/')
    month = int(m)
    day = int(d)
    year = int(y)
    # Put data into list
    datalist.append([day, month, year, lowtemp, hightemp, rainfall])
    
# Close File
infile.close()

# Analyze Data
# Get date of interest
month = int(input("For the date you care about, enter the month: "))
day = int(input("For the date you care about, enter the day: "))

# Find historical data
gooddata = []
for singleday in datalist:
    if (singleday[0] == day) and (singleday[1] == month):
     gooddata.append([singleday[2], singleday[3], singleday[4], singleday[5]])   

# Analyze historical data
minsofar = 120 # set to extreme high value
maxsofar = -100 # set to extreme low value
numgooddates = 0
sumofmin = 0
sumofmax = 0
for singleday in gooddata: # loop through data and update values
    numgooddates =+ 1
    sumofmin =+ singleday[1]
    sumofmax += singleday[2]
    if singleday[1] < minsofar:
        minsofar = singleday[1]
    if singleday[2] > maxsofar:
        maxsofar = singleday[2]
        
avglow = sumofmin / numgooddates # calculate avg min
avghigh =sumofmax / numgooddates # calculate avg max

# Present Data
print("There were",numgooddates,"days")
print("The lowest temperature on record was", minsofar)
print("The highest temperature on record was", maxsofar)
print("The avearge low has been", avglow)
print("The aveage high has been", avghigh)

# Here we kept the design manageable and organized