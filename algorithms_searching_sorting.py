# -*- coding: utf-8 -*-
"""
Created on Tue Jan 28 17:12:57 2020

@author: Richard Chukwu
"""
### ALGORITHMS: SEARCHING AND SORTING ################
# Algorithm: Precise set of steps or rules to follow to accomplish some task
# the program is the concrete incarnation of the more general algorithm.
# Important part is coming with the steps to follow.

## Understanding Algorithms
## How they are described
## How they are implemented
## Searching
## Sorting

# Ingredients is the data(using the general nature of the ingredients)
# Algorithm is the sequence of steps to follow.
## An algorithm can be thought of as a more general description that's not necessarily tied directly to a particular programming language

# in a flowchart, an algorithm is described graphically
# Another option is to use a pseudocode. in pseudocode we give an overview of the various steps the algorithm will take and how they relate to eachother.
# An example of pseudo code:
# step 1
# step 2
# If (condition)
    #step 3
    #go to step 1
# else
    #step 4
    
# A sibgle step might involve a series of code. Well designed algorithms should be easy to convert to code

## Search Algorithm
#Assume we have a list which is just a collection of values
    ## Try to find out whether a particular value is in a list or not
    ## The search will return either True or False
    
## We need to pieces of data
## Input:
    # list of values: L
    # Value to find: v
    
## Output
    # True if v is in L
    # false otherwise
    
## The idea is simple here, we are just gonna go through each element oof the list, checking to see if there is a match - this is called a linear search. we are just going down the line looking at each item one at a time. If we find a match we return true, otherwise we return false
## In pseudocode:
    # Let i be the index of the first element in the list
    # while i is less than the size of the list
    # while i is less than the size of the list:
        # If element i of list l matches v, return True
    
list_search = [25, 28, 1, 3, 6, 9, 8, 7, 4, 24, 11]

i = 0
while i < len(list_search):
    if list_search[i] == 11:
        print("True")
    i += 1
    
## A better way
def isIn(L, v):
    i = 0 # initialize index to zero
    while (i < len(L)):# if statement for comparison
        if L[i] == v:
            return True
        else:
            i += 1
    return False
    
favourite_foods = ['pizza', 'barbeque', 'gumbo', 'chicken and dumplings', 'pecan pie', 'ice cream']

isIn(favourite_foods, "gumbo")

## the built in python module that implements this for us is the in command.
print('gumbo' in favourite_foods)
print('coconut' in favourite_foods)

## lets assume that instead of having the values of a list in any order,our list was sorted from smallest to largest. A command to sort the list will be to call the sort method on the list. Bit for purposes of sorting the algorithm we can just assume that the list has already been sorted.Now is there a better way that we might write the search routine?

## If we had a dictionary and had to create a program to look up a word, how would you do it? going through every word to see if it matches would be inefficient. A more efficient approach is to start by check some point in the middle of the dict. If that's not the word you are looking for, then you figure out whether it came before or after the word you wanted to fine. And then look in the remaining half of the dictionary. You can continue this until you've found the value or else found that it was missing.
## In pseudocode:
## Input:
    # List of values in sorted order: L
    # value to find: v
## Output:
    #True if v is in L, False otherwise
    
# Our approach would be to gradually narrow down the range of options until we find the one we are looking for. So at any point we have a max and min index of where the value might be.
    # At the very beginning: set low = 0 and high = length of list - 1
    # If L[low] == v or L[high] == v, return True # at this point we know our value is between high and low.
    # While low < high -1 # value is between L[low] and L[high]
        # A. midpoint = low + (high-low)/2 # integer division which is at least 1. This guarantees that the mid point is not the same as low or high.
        # B. if L[midpoint] == v return True
        # C. if L[midpoint] < v, set low = midpoint
        # D. else set high = midpoint
    # Return false
    # Notice that in each iteration of the loop,we still have that same condition. The value we are looking for is either between element low and element high or else it's not in the list. this condition, this thing that is the same every time we go through a loop is called a loop invariant. Whe  we are designing an algorithm, it is always helpful if we can identify such a loop invariant. Now we need to decide what is don at each iteration of the loop itself. We compute a midpoint
# The term for the above search is a binary search. reducing the search range by a factor of two at each iteration.  It is much more efficient than the linear search where we looked at each item, one after the other.  
## In python:
def binaryIn(L, v):
    low = 0
    high = len(L) - 1
    if L[low] == v or L[high] == v:
        return True
    while low < (high - 1):
        midpoint = low + (high-low)//2
        if L[midpoint] == v:
            return True
        elif L[midpoint] < v:
            low = midpoint
        else:
            high = midpoint
    return False


print(binaryIn(favourite_foods, 'gumbo'))
print(binaryIn(favourite_foods, 'coconut'))

## to handle the possiblitiy of an empty list, we made one modification at the beginning.

def binaryIn(L, v):
    if len(L) < 0:
        return False
    low = 0
    high = len(L) - 1
    if L[low] == v or L[high] == v:
        return True
    while low < (high - 1):
        midpoint = low + (high-low)//2
        if L[midpoint] == v:
            return True
        elif L[midpoint] < v:
            low = midpoint
        else:
            high = midpoint
    return False
            
########### Questions to address: #############
# What if we had just one element or just two
# What if the thing we were looking for was the very first one, or the very last one
# What if it was a giant list?   

## To know where it is in the list i.e to return the index
    ## Input:
    # List of values in sorted order: L
    # value to find: v
## Output:
    #Index of entry that's equal to v, if v is in L
    # -1 if v is not in L
    
# Our approach would be to gradually narrow down the range of options until we find the one we are looking for. So at any point we have a max and min index of where the value might be.
    # At the very beginning: set low = 0 and high = length of list - 1
    # If L[low] == v return [low] or if L[high] == v, return [high]# at this point we know our value is between high and low.
    # While low < high -1 # value is between L[low] and L[high]
        # A. midpoint = low + (high-low)/2 # integer division which is at least 1. This guarantees that the mid point is not the same as low or high.
        # B. if L[midpoint] == v return midpoint
        # C. if L[midpoint] < v, set low = midpoint
        # D. else set high = midpoint
    # Return -1

def binaryIn(L, v):
    if len(L) < 0:
        return False
    low = 0
    high = len(L) - 1
    if L[low] == v:
        return [low]
    elif L[high] == v:
        return [high]
    while low < (high - 1):
        midpoint = low + (high-low)//2
        if L[midpoint] == v:
            return [midpoint]
        elif L[midpoint] < v:
            low = midpoint
        else:
            high = midpoint
    return -1

print(binaryIn(favourite_foods, 'gumbo'))
print(binaryIn(favourite_foods, 'coconut'))

### SLIGHTLY MORE COMPLEX ALGORITHMS ###########
######### SORTS ###################
# There a lot of ways to sort.
# What if we had a list of values in no particular order? to find values in that list we are stuck using a linear search. If we had a sorted list though, we could use a binary search. sorting makes our operations easier.

## selection sort works like this. We start with a mixed up set of values that we want to put in order from smallest to largest.
selection_list = [63, 18, 42, 7, 99, 26, 71]
# First we look through all the values to find the smallest one and put that into the first place. then we repeat the process to find the next smallest one and put it into the secon place. Each time we have to look through all the remaining items to select the one that is the smallest remaining - which gives the name selection.
# Do we move around the elements of the list or make a copy? since lists are mutable. We could either do in place sorting( directly sorting the elements of a given list) or an out of place sort( making a copy of a given list and then sorting the copy, leaving the original list in place.). Choosing depends on wether you wanna maintain the original order.
## the pseudocode is as follows:
#input: unsorted list L
# Output: L, with elemets sorted smallest to largest

#1.maxindex = length of L - 1
#2. for i = 0 to maxindex
    #a. find the index with the smallest value between i and maxindex
    #b. swap that element with the elemet in index i
    
## In python
def selectionSort(L):
    maxindex = len(L) - 1
    for i in range(0, maxindex-1): # go through the full length of the list
        #find the smallest remaining
        min_index = i
        for j in range(i+1, maxindex+1):
            if L[j] < L[min_index]:
                min_index = j
        # swap that item
        temp = L[i]
        L[i] = L[min_index]
        L[min_index] = temp
favorite_foods = ['pizza', 'barbeque', 'gumbo', 'chicken and dumplings', 'pecan pie', 'ice cream']
selectionSort(favorite_foods)
print(favorite_foods)
            
## Insertion sort
# For each iteration "n", sort the first "n" elements
# Each iteration add 1 to "n"
# we start with the first item, take secon and compare with the first, one by one
## Input: insorted list L
## output: L, with elements sorted smallest to largest

#1.For i = 0 to length of L - 1
    # 1. temp = L[i] # element i at each iteration. we copy the element into temp
    # j = i - 1 # at each iteration we need to work our way back or down the list
    # While j >= 0 and L[j]> temp # check if j is greater than the element we are trying to insert
        #1. l[j+1] = L[j]
        #2. j = j - 1
    #4. l[j+1] = temp
    
def insertionSort(L):
    for i in range(0, len(L)):
        temp = L[i]
        j = i-1
        while(j >= 0) and (L[j] > temp):
            L[j+1] = L[j]
            j -= 1
        L[j+1] = temp
favorite_foods = ['pizza', 'barbeque', 'gumbo', 'chicken and dumplings', 'pecan pie', 'ice cream']
insertionSort(favorite_foods)
print(favorite_foods)
   
# Python has in built sorting  functions
favorite_foods.sort() # this is an in-place sort  
print(favorite_foods)       

sorted_favourites = sorted(favorite_foods) # this is an out-of-place sort
# we can also set the reverse = True.
# python sort() combines insertion sort with merge sort