# -*- coding: utf-8 -*-
"""
Created on Tue Jan 28 02:45:29 2020

@author: Richard Chukwu
"""
### Data Structures: Ways to organize large amounts of the same type of data

# list(array) is horizontally structured
# Grid
# Nonlinear or sequential.

## Organization affects operations( military queue)

## Data Structures
    # Stacks and Queues can be implemented using lists
    # Dictionaries and sets can be implemented using a hash table
    
## Imaging a stack of books when we can remove from the top or the bottom
    #Push: Adding something new to the stack
    #Pop: Removing an item from the top of the stack 
    
class book:
    title = ""
    author = ""
    
long_book = book()
long_book.title = "War and Peace"
long_book.author = "Tolstoy"

medium_book = book()
medium_book.title = "Book of Armaments"
medium_book.author = "Maybard"

short_book  = book()
short_book.title = "Vegetable I like"
short_book.author = "John Keyser"

## Stacks are represented using a list
# first book in the list is on the bottom
# Last book in the list is on the top
# "Pushing" a book on to the stack requires using the append command on the list

book_stack = [] # treated as a list in memory with medium first and long last. conceptually, its a stck with the long on top and botton below

book_stack.append(medium_book)
book_stack.append(short_book)
book_stack.append(long_book)

next_book = book_stack.pop()
print(next_book.title+" by "+next_book.author) # think of this as a stack even tho we implement this as a list
## we could create a stack class with methods puch and pop

class book:
    title = ""
    author = ""
class Stack(book):
    _stack = []
    def push(self, item):
        self._stack.append(item)
    def pop(self):
        return self._stack.pop()
    
long_book = book()
long_book.title = "War and Peace"
long_book.author = "Tolstoy"

medium_book = book()
medium_book.title = "Book of Armaments"
medium_book.author = "Maybard"

short_book  = book()
short_book.title = "Vegetable I like"
short_book.author = "John Keyser"


book_stack = Stack()
book_stack.push(medium_book)
book_stack.push(short_book)
book_stack.push(long_book)

next_book = book_stack.pop()
print(next_book.title+" by "+next_book.author)
 # a stack is a last in pop out
# solitaire is a good example of a stack. in solitaire you draw cards from a deck and as cards are drawn, they are placed onto a stack called waste. we would say they get pushed on to the waste stack. During the player's turn, they always have the option of taken the current top card off the waste stack to play it. We would say the top card gets popped off the waste stack.
 
### Code for waste stack ###
class Stack(book):
    _stack = []
    def push(self, item):
        self._stack.append(item)
    def pop(self):
        return self._stack.pop()
    
waste_pile = Stack()
# Setting up solitaire game here

while not(noTurnsLeft()):
    # get the player's move
    
    if move == "Draw":
    # Get next three cards and push them onto waste stack
        next_card1 = draw_next_card()
        next_card2 = draw_next_card()
        next_card3 = draw_next_card()
        waste_pile.push(next_card1)
        waste_pile.push(next_card2)
        waste_pile.push(next_card3)

    elif move == "PlayfromWaste":
        #Player wants to play the top card from the waste pile
        current_card = waste_pile.pop()
        #Have player play current_card
        
# inside computers stacks have a very fundamental use. As we make function calls the computer memory is storing data in the call stack(control stack, runtime stack,frame stack) The call stack consists of function activation records, where the FAR keeps track of all the variables and data defined in that part of the program.

## The opposite of a stack is a queue. we can implement a list in a queue.
## order of queue is same as list order
## Can push new objects on to the end of the list using the append command
## Instead of popping from the end of the list, an element is taken off the front
        
## The pop command here takes a parameter, indicating which element gets taken off the list. If no parameter is given, it defaults to the final element.
## To take off the first element, we pass in 0.
## For example
book_queue = [] # treated as a list in memory with medium first and long last. conceptually, its a stck with the long on top and botton below

book_queue.append(medium_book)
book_queue.append(short_book)
book_queue.append(long_book)

next_book = book_queue.pop(0) # always call with parameter 0
print(next_book.title+" by "+next_book.author)

## Creating a queue class
class Queue:
    _queue = []
    def enqueue(self, item):
        self._queue.append(item)
    def dequeue(self, item):
        return self._queue.pop(0)
    def isEmpty(self): #method to tell if the queue is empty
        return(len(self._queue) == 0)

### Suppose we have a business where we sell strawberries. The business has a list of orders which we wanna process on a first come first served basis for as long as the inventory holds out. We implement it as such
from random import randint
class Queue:
    _queue = []
    def enqueue(self, item):
        self._queue.append(item)
    def dequeue(self):
        return self._queue.pop(0)
    def isEmpty(self): #method to tell if the queue is empty
        return(len(self._queue) == 0)
        
class Order: #class defining the holding order
    def __init__(self,  customer = "", amt = 0):#amt is no of strawberries orders
        self._customer = customer
        self._amount = amt
    def customer(self):
        return self._customer
    def numOrdered(self):
        return self._amount
    
orders = Queue()
for ordernum in range(20): #generating random orders
    amount = randint(1,200)
    customer = "Customer "+str(ordernum)
    neworder = Order(customer, amount)
    orders.enqueue(neworder)
    
inventory = 1000
while not orders.isEmpty():
    order = orders.dequeue()
    if order.numOrdered() < inventory:
        # We can fill the order
        print("Fill order for", order.numOrdered(), "strawberries for customer", order.customer())
        inventory -= order.numOrdered()
    else:
        print("Notify", order.customer(), "that we cannot fulfil the order")

exit()

### Another place where queue comes up is in a buffer. A buffer is queue of software events, like mouse movements or a keyboard key being pressed. This is so you dont lose your data. 

### Hash Table ###
#A hash table maps a large number of data values into a smaller number of indices.
#Imagine you have friends with fone numbers but the all have blocked caller ids so when they call you dont know whose fone number belongs to whom. Instead of a list of 10 million elements lets instead have a list of 100 elements and store each person in just a slot that corresponds to just the last two digits of the phone number. Chaining makes a list of elements with the same number or in the same slot.
## To convert name to number, we use a hash function. A hash function is a function used to convert a key phrase into a number that can be usedto index into an array

# In python, the dictionary implements a hash table for us and we dont have to come up with our own hash function.
# dict() is a command to create a new dictionary. Other names for a hash table include map, symbol and associative array. We use curly braces to create a dictionary in python.

my_dictionary = {}
nicknames = {"superstar" : "sue smith",
             "cowboysfan" : "bill brown",
             "JJwin" : "John James"}

print(nicknames["cowboysfan"])

##OR

nicknames = {}
nicknames["superstar"] = "Sue Smith"
nicknames["cowboysfan"] = "Bill Brown"
print(nicknames["superstar"])
del nicknames["superstar"] ##square bracket key
print("cowboysfan" in nicknames)## to check if a key is in the dict

# Dictionaries require two parts: A key (any immutable data type, tuple) and a value:can be mutable or immutable

#You can also iterate in dict using a for statement
for nickname in nicknames:
    print("The nickname for "+nicknames[nickname]+" is "+nickname)
    
## A program to handle passwords:
    
passwords = {"richie" : "hannibal", "Sue" : "PaSsWoRd", "Bill" : "G9.kf-21.-fe8ilfb"}

failed_attempts = 0
verified = False # used to know if someone is logged in correctly

while (not verified): # run as long as we are not verified
    username = input("What is your username? ")
    password = input("What is your password? ")
    if (username in passwords) and (passwords[username] == password):
        print ("Welcome!")
        verified = True
    else:
        print("Invalid username/password combination")
        failed_attempts += 1
        if failed_attempts > 2:
            print("Too many incorrect attempts. Goodbye")
            exit()
            
#### SET ###
#A collection of items stored using a hash table does mathematical set operations - checks membership very quickly
#for example
ingredients = {'flour', 'sugar', 'eggs', 'milk'} # no key value pairs

people = {'John', 'Sue', 'Bill'} # created a new set people and initialized it with three names
if 'John' in people:
    print("yes")
else:
    print("No!")
    
##OR
    
people = set(['John', 'Sue', 'Bill']) # can also take in a list. We can use it to create an empty set initialized it with three names
if 'John' in people:
    print("yes")
else:
    print("No!")
## set operations include    
work_friends = {'Sue', 'Eric', 'Fred'}

print(work_friends)
work_friends.add('Kathy')
print(work_friends)
work_friends.remove('Fred')
print(work_friends)

## Supports set operations
neighborhood_friends = set(['John', 'Sue','Bill'])
work_friends = {'Sue', 'Eric', 'Fred'}

print(neighborhood_friends - work_friends)# in one and not the other
print(neighborhood_friends | work_friends)# union
print(neighborhood_friends & work_friends)# intersection(people in first and secon)
print(neighborhood_friends ^ work_friends)# people in either but not in both(exclusive or...union - intersection)

## Usefulness of sets
# here is the set of items you need to buy:
{'Yeast', 'Eggs', 'Flour', 'Peppers'}

class recipe:
    name = ''
    ingredients = []
    def __init__(self, name, ingredients):
        self.name = name
        self.ingredients = ingredients
        
dish1 = recipe("omlette", ['Eggs', 'Tomatoes', 'Onions', 'Peppers'])
dish2 = recipe("Bread", ['Flour', 'Yeast'])
dish3 = recipe("cake", ['Eggs', 'Flour', 'Sugar', 'Butter'])
dishes_to_fix =[dish1, dish2, dish3]

shopping_list = set()# start out with an emty set
for dish in dishes_to_fix:
    ingredients = set(dish.ingredients)
    shopping_list = shopping_list | ingredients
    
ingredients_on_hand = {'Onions', 'Milk', 'Butter', 'Honey', 'Oatmeal', 'Sugar', 'Tomatoes'}

shopping_list -= ingredients_on_hand # set difference

print("Here is the set of items you need to buy:")
print(shopping_list)

## Linear, Sequential (using list): Stacks and queues
## Nonlinear, non sequential(usinh hash table): dictionaries and sets
# By providing some structure we can speed up operations.

