# -*- coding: utf-8 -*-
"""
Created on Mon Feb  3 23:12:36 2020

@author: User
"""

from sklearn.feature_extraction.text import CountVectorizer
from sklearn.feature_extraction.text import TfidfVectorizer
import seaborn as sns

corpus = ['Time flies like an arrow.',
'Fruit flies like a banana.']


one_hot_vectorizer = CountVectorizer(binary=True)
one_hot = one_hot_vectorizer.fit_transform(corpus).toarray()
vectorizer = TfidfVectorizer()
tfidf = vectorizer.fit_transform(corpus).toarray()
vocab = vectorizer.get_feature_names()
sns.heatmap(one_hot, annot=True,
cbar=False, xticklabels=vocab,
yticklabels=['Sentence 2'])
sns.heatmap(tfidf, annot=True, cbar=False, xticklabels=vocab,
yticklabels= ['Sentence 1', 'Sentence 2'])



from keras.preprocessing.text import Tokenizer
corpus = 'Time flies like an arrow.'
corpus = corpus.lower()
tokenizer = Tokenizer(char_level = False, filters = '')
tokenizer.fit_on_texts([corpus])
token_list = tokenizer.texts_to_sequences([corpus])

one_hot_vectorizer = CountVectorizer(binary=True)
one_hot = one_hot_vectorizer.fit_transform(corpus).toarray()
vectorizer = TfidfVectorizer()
tfidf = vectorizer.fit_transform(corpus).toarray()
vocab = vectorizer.get_feature_names()
sns.heatmap(one_hot, annot=True,
cbar=False, xticklabels=vocab,
yticklabels=['Sentence 2'])
sns.heatmap(tfidf, annot=True, cbar=False, xticklabels=vocab,
yticklabels= ['Sentence 1', 'Sentence 2'])