# -*- coding: utf-8 -*-
"""
Created on Sat Jan 25 12:40:03 2020

@author: Richard chukwu
"""
####################### #GAME DESIGN# #############################
######### HOW TO DEVELOP A GAME #####################
########## Functions allow a top down design approach #############
####### GRID BASED MATCHING GAME DESIGN ############

# in procedural programming, we set up functions to handle all the main tasks.These little place holder functions are called stubs. They dont do what they are intended to do but dont cause the program to crash
def Initialize():
    # Initialize game
    print("Initializing")
    
def ContinueGame():
    # Return false if game should end, true if game is not over
    print("Checking to see if we should continue")
    return True # false returns just two lines

def DoRound():
    # Perform one round of the game
    print("Doing one round")

# here we are stubbing out the program.
    
# Initialize and set up
Initialize()
# Loop to make sure it's not time to end the game:
# While game is not over
score = 50
goalscore = 100
while ContinueGame():
    # Do a round of the game
    DoRound()
# Go through one round of the game
# We have just concluded the foundation. Now we add another layer.
    
 def Initialize():
    # Initialize game
    print("Initializing")
    
def ContinueGame(current_score, goal_score = 100):
    # Return false if game should end, true if game is not over
    if (current_score >= goal_score):
        return False
    else:
        return True
    print("Checking to see if we should continue")
    

def DoRound():
    # Perform one round of the game
    print("Doing one round")

# here we are stubbing out the program.
    
# Initialize and set up
# Here we set up the grid with an initial arrangement of letters
# Set the user's score to zero
# Set initial values for other variables, such as the number of rounds
Initialize()
# Loop to make sure it's not time to end the game:
### Go Through One Round of the Game
## Get the move from the user
## Update the game based on that move
## Show the user the new state of the grid
## if score is greater than goal score, return false, otherwise return true.


# While game is not over
while ContinueGame(score, goalscore):
    # Do a round of the game
    DoRound()   
    
# How will the grid be designed? We need to build the data structure. We use a grid representation which is a list of lists.
    

# next we turn to our initialization function
def InitializeGrid(board):
    # Initialize Grid by reading in from file
    print("Initializing")
    
def Initialize(board):
    # Initialize game
    # Initialize grid
    InitializeGrid(board)
    #Initialize score
    global score # using global for our immutable variable
    score = 0
    # Initialize turn number
    global turn
    turn = 1
    
# State main variables
score = 0
turn = 0
goalscore = 100

board = [[0, 0, 0, 0, 0, 0, 0, 0],
         [0, 0, 0, 0, 0, 0, 0, 0],
         [0, 0, 0, 0, 0, 0, 0, 0],
         [0, 0, 0, 0, 0, 0, 0, 0],
         [0, 0, 0, 0, 0, 0, 0, 0],
         [0, 0, 0, 0, 0, 0, 0, 0],
         [0, 0, 0, 0, 0, 0, 0, 0],
         [0, 0, 0, 0, 0, 0, 0, 0]]

##### To assign random objects to each grid cell, we use the random module, part of the standard library. We import the choice function which will choose one elemenet from a list:
def InitializeGrid(board):
    # Initialize Grid by random value
    for i in range(8):
        for j in range(8):
            board[i][j] = choice(['Q', 'R', 'S', 'T', 'U'])
    print("Initializing")
 
### Go through one round of the game
# Show the user the new state of the grid
# Get the move from the user
# determine the results of that move
# Increase turn number by 1
    
def DrawBoard(board):
    # Display the board to the screen
    print("Drawing Board")
    
def getMove():
    # Get the move from the user
    print("Getting move")
    return "blu"

def Update(board, move):
    # Update the board according to the move
    print("Updating board")
    
def doRound(board):  # DONE
    #Perform one round of the game
    #Display current board
    DrawBoard(board)
    #Get move
    move = GetMove()
    #Update board
    Update(board, move)
    # Update turn number
    global turn
    turn += 1
    
##### next routine is to present the board. Draw horizontal and vertical lines to separate the individual elements
 def DrawBoard(board):
    # Display the board to the screen
    linetodraw = ""
    # Draw some blank lines first to separate our display from any that might have appeared right above it
    print("\n\n\n")
    print(" ----------------------------------") #Then we draw a            horizontal line
    # Now draw rows from 8 down to 1
    for i in range (7, -1, -1):
        # Draw each row
        linetodraw = ""
        for j in range(8):
            linetodraw += " | " + board[i][j] # for each column we out put a vertical line followed by a letter designating the object we have
        linetodraw += " |" # print one vertical line to end the string
        print(linetodraw)
        print("-----------------------------------")   
        
# Next we ask the user for a move
def GetMove():
    # get the move from the user
    move = input("Enter move: ")
    return move

### The Turn Routine
# Swap pieces according to move
# Repeat until no eliminations occur
    # Remove pieces 3-in-a-row, or 3-in-a-column
    # Dorp remaining objects down
    
# Stubbing
## Swap Pieces
# Convert move into:
    # a current position
    # an adjacent position
    
# code for moves
    # Space for the letter being swapped(e3)
# Direction of the swap (u, d, l, r)
# Swap the pieces
    
def ConvertLetterToCol(col):
    if col == 'a':
        return 0
    elif Col == 'b':
        return 1
    elif Col == 'c':
        return 2
    elif Col == 'd':
        return 3
    elif Col == 'e':
        return 4
    elif Col == 'f':
        return 5
    elif Col == 'g':
        return 6
    elif Col == 'h':
        return 7
    else:
        #not a valid column!
        return -1
def SwapPieces(board, move):
    # Swap pieces on board according to move
    # get original position
    origrow = int(move[1])-1 # convert the second element of the move to an integer and subtract 1 since our indices run 0 to 7
    origcol = ConvertLetterToCol(move[0]) 
    
    # Get adjacent position
    if move[2] == 'u':
        newrow = origrow + 1
        newcol = origcol
    elif move[2] == 'd':
        newrow = origrow - 1
        newcol = origcol
    elif move[2] =='l':
        newrow = origrow
        newcol = origcol - 1
    elif move[2] == 'r':
        newrow = origrow
        newcol = origcol + 1
    
    # Swap objects into position
    temp = board[origrow][origcol] # copy one into a temporary position
    board[origrow][origcol] = board[newrow][newcol] #move the other object into the original position
    board[newrow][newcol] = temp
    print("Swapping Pieces")
    
    # Get adjacent position
    # Swap objects in two positions


####### The Turn Routine
# Swap pieces according to move
# Repeat until no eliminations occur
    # Remove pieces 3-in-a-row, or 3-in-a-column
    # Dorp remaining objects down
### Generate another board to track whether pieces should be removed or not
    # Initialize it to "not"
## Go through each row, looking for 3 in a row of the same
    # If found, mark those three as to be removed
## Go through each column, looking for 3 in a column of the same
    # If found, mark those theree as to be removed
## Go through entire board and change pieces to be removed to 0
    # Increment score for each one removed
## If any pieces removed, return "True", otherwise return "False"
def RemovePieces(board):
    # remove 3-in-a-row and 3-in-a-coulumn pieces
    # Create board to remove-or-not
    remove = [[0, 0, 0, 0, 0, 0, 0, 0],
              [0, 0, 0, 0, 0, 0, 0, 0],
              [0, 0, 0, 0, 0, 0, 0, 0],
              [0, 0, 0, 0, 0, 0, 0, 0],
              [0, 0, 0, 0, 0, 0, 0, 0],
              [0, 0, 0, 0, 0, 0, 0, 0],
              [0, 0, 0, 0, 0, 0, 0, 0],
              [0, 0, 0, 0, 0, 0, 0, 0]]
    # Go through rows
    for i in range(8):
        for j in range(6):# the first 6 columns
            if (board[i][j] == board[i][j+1]) and (board[i][j] == board[i][j+2]):
                # three in a row are the same
                remove[i][j] = 1;
                remove[i][j+1] = 1;
                remove[i][j+2] =1;
    # go through columns
    for j in range(8):
        for i in range(6):
            if (board[i][j] == board[i+1][j]) and (board[i][j] == board[i+2][j]):
                # three in a row are the same
                remove[i][j] = 1;
                remove[i][j+1] = 1;
                remove[i][j+2] =1;
    # Eliminate those marked
    global score
    removed any = False
    for i in range (8):
        for j in range(8):
            if remove[i][j] == 1:
                board[i][j] = 0
                score += 1
                removed_any = True
    return removed_any
    print("Removing pieces)


######### DROPPING PIECES #########
## Go through column from bottom to top
## Make list of non-zero elements
## copy those elements in from bottom to top
## Put 0s at top
def DropPieces(board):
    # Drop pieces to fill in blanks
    for j in range(8):
        # make list of pieces in the column
        listofpieces = []
        for i in range(8):
            if board[i][j] != 0:
                listofpieces.append(board[i][j])
    # Copy that list into column
    for i in range(len(listofpieces)):
        board[i][j] = listofpieces[i]
    # fill in remainder of column with 0s
    for i in range(len(listofpieces), 8):
        board[i][j] = 0
    #Drop pieces to fill in blanks
    print("Dropping Pieces")

############ FILLING BLANKS ###############
# Loop through all elements
# If zero, generate a random new piece for that position    
def FillBlanks(board):
    # Fill blanks with random pieces
    for i in range(8):
        for j in range(8):
            if (board[i][j] == 0):
                board[i][j] = choice(['Q', 'R', 'S','T', 'U'])
    print("Filling blanks")
    
def Update(board, move):
    # Update the board according to move
    SwapPieces(board, move)
    pieces_eliminated = True
    while pieces_eliminated:
        pieces_eliminated = RemovePieces(board)
        DropPieces(board)
        FillBlanks(board)
    