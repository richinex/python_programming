# -*- coding: utf-8 -*-
"""
Created on Wed Feb 12 23:48:31 2020

@author: User
"""

### NUMERICAL PYTHON ##################
# Arrays are created and treated according to certain rules, and as a programmer,you may direct Python to compute and handle arrays as a whole, or as individual array elements. All array elements must be of the same type, e.g., all integers or all floating point numbers.
# linspace function from numpy could be used to generate an array of evenly distributed numbers from an interval [a, b]. linspace function from numpy could be used to generate an array of evenly distributed numbers from an interval [a, b].

from numpy import linspace
x = linspace(0, 2, 3) # a space is allocated in memory for the array.
x

type(x) # check type of array as a whole
# numpy.ndarray
type(x[0]) # check type of array element (zero based indexing)
#numpy.float64

## Length and Zeros
from numpy import zeros
x = zeros(3, int) # or dtype = int get array with integer zeros. This command instructs python to reserve or allocate space in memory for the array produced by zeros
x
y = zeros(3) # get array with floating point zeros
y

y[0] = 0.0; y[1] = 1.0; y[2] = 2.0 # overwrite
y
In [8]: len(y)
Out[8]: 3

## The Array function
from numpy import array
x = array([0, 1, 2]) # get array with integers
x

x = array([0., 1., 2.]) # get array with real numbers. use dots to get floating numbers

## arrays are mutable. to copy an array, you may use the copy function from numpy,
from numpy import copy
x = linspace(0, 2, 3) # x becomes array([ 0., 1., 2.])
y = copy(x)
y
array([ 0., 1., 2.])
y[0] = 10.0
y
#array([ 10., 1., 2.]) # ...changed
x
#Out[21]: array([ 0., 1., 2.]) # ...unchanged

## TWO-DIMENSIONAL ARRAYS AND MATRIX COMPUTATIONS ##
import numpy as np
I = np.zeros((3, 3)) # create matrix (note parentheses!)
I

In [4]: type(I) # confirm that type is ndarray
# Out[4]: numpy.ndarray

I[0, 0] = 1.0; I[1, 1] = 1.0; I[2, 2] = 1.0 # identity matrix
x = np.array([1.0, 2.0, 3.0]) # create vector
y = np.dot(I, x) # computes matrix-vector product
y
#Out[8]: array([ 1., 2., 3.])

## MATRIX PRODUCT ####
# There is another way to handle matrices and vectors with NumPy, which will appear more like you are used to. For example, a matrix-vector product is then coded as A*x and not by use of the dot function. To achieve this, we must use objects of another type, i.e., matrix objects (note that a matrix object will have different properties than an ndarray object!). If we do the same matrix-vector calculation as above, we can show how ndarray objects may be converted into matrix objects and how the calculations then can be fulfilled:

import numpy as np
I = np.eye(3) # create identity matrix
I
#Out[3]:
#array([[ 1., 0., 0.],
#[ 0., 1., 0.],
#[ 0., 0., 1.]])
type(I) # confirm that type is ndarray
#Out[4]: numpy.ndarray
I = np.matrix(I) # convert to matrix object
type(I) # confirm that type is matrix
#Out[6]: numpy.matrixlib.defmatrix.matrix
x = np.array([1.0, 2.0, 3.0]) # create ndarray vector
x = np.matrix(x) # convert to matrix object (row vector)
x = x.transpose() # convert to column vector
y = I*x # computes matrix-vector product
y
#Out[11]:
#matrix([[ 1.],
#        [ 2.],
#        [ 3.]])
# Note that np.matrix(x) turns x, with type ndarray, into a row vector by default (type matrix), so x must be transposed with x.transpose() before it can be multiplied with the matrix I.

### PSEUDO RANDOM NUMBERS ###################
#Programming languages usually offer ways to produce (apparently) random numbers, referred to as pseudo-random numbers. These numbers are not truly random, since they are produced in a predictable way once a “seed” has been set (the seed is a number, which generation depends on the current time).

import random

a = 1; b = 6
r1 = random.randint(a, b) 	# first die
r2 = random.randint(a, b) 	# second die
print('The dice gave:  {:d} and {:d}'.format(r1, r2)) # returns a pseudo-random integer on the interval [a, b], a ≤ b
# When debugging programs that involve pseudo-random number generation, it is a great advantage to fix the seed, which ensures that the very same sequence of numbers will be generated each time the code is run
import random
random.seed(10)
a = 1; b = 6
r1 = random.randint(a, b) 	# first die
r2 = random.randint(a, b) 	# second die
print('The dice gave:  {:d} and {:d}'.format(r1, r2))

#The random module contains also other useful functions, two of which are random (yes, same name as the module) and uniform. Both of these functions return a floating point number from an interval where each number has equal probability of being drawn. For random, the interval is always [0, 1) (i.e. 0 is included, but 1 is not), while uniform requires the programmer to specify the interval [a, b] (where both a and b are included14). The functions are used similarly to randint, so, interactively, we may for example do:
import random
x = random.random() # draw float from [0, 1), assign to x
y = random.uniform(10, 20) # ...float from [10, 20], assign to y
print('x = {:g}, y = {:g}'.format(x, y))
#x = 0.714621 , y = 13.1233

#If you need many pseudo-random numbers, one option is to use such function calls inside a loop (Chap. 3). Another (faster) alternative, is to rather use functions that allow vectorized drawing of the numbers, so that a single function call provides all the numbers you need in one go. Such functionality is offered by another module, which also happens to be called random, but which resides in the numpy library. All three functions demonstrated above have their counterparts in numpy and we might show interactively how each of these can be used to generate, e.g., four numbers with one function call.
import numpy as np
np.random.randint(1, 6, 4) # ...4 integers from [1, 6)
#Out[2]: array([1, 3, 5, 3])
np.random.random(4) # ...4 floats from [0, 1)
#Out[3]: array([ 0.79183276, 0.01398365, 0.04982849, 0.11630963])
np.random.uniform(10, 20, 4) # ...4 floats from [10, 20)
#Out[4]: array([ 10.95846078, 17.3971301 , 19.73964488, 18.14332234])

# In each case, the size argument is here set to 4 and an array is returned. Of course, with the size argument, you may ask for thousands of numbers if you like. As is evident from the interval specifications in the code, none of these functions include the upper interval limit. However, if we wanted, e.g., randint to have 6 as the inclusive upper limit, we could simply give 7 as the second argument in stead.

#### SHUFFLE #########
#If you have an array15 with numbers, you can shuffle those numbers in a randomized way with the shuffle function,
import numpy as np
a = np.array([1, 2, 3, 4])
In [3]: np.random.shuffle(a)
In [4]: a
# Out[4]: array([1, 3, 4, 2])
# Note that also numpy allows the seed to be set. For example, setting the seed to 10 (as above), could be done by
np.random.seed(10)

# The fact that a module by the name random is found both in the standard Python library random and in numpy, calls for some alertness. With proper import statements (discussed in Sect. 1.4.1), however, there should be no problem.