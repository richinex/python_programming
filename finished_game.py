# -*- coding: utf-8 -*-
"""
Created on Sat Jan 25 13:51:24 2020

@author: Richard Chukwu

####### GAME DESIGN ################
"""
from random import choice

def InitializeGrid(board):
    # Initialize Grid by random value
    for i in range(8):
        for j in range(8):
            board[i][j] = choice(['Q', 'R', 'S', 'T', 'U'])
    print("Initializing")
    
def Initialize(board):
    # Initialize game
    # Initialize grid
    InitializeGrid(board)
    #Initialize score
    global score # using global for our immutable variable
    score = 0
    # Initialize turn number
    global turn
    turn = 1
    
def ContinueGame(current_score, goal_score = 100):
    # Return false if game should end, true if game is not over
    if (current_score >= goal_score):
        return False
    else:
        return True

def DrawBoard(board):
    # Display the board to the screen
    linetodraw = ""
    # Draw some blank lines first to separate our display from any that might have appeared right above it
    print("\n\n\n")
    print(" ----------------------------------") #Then we draw a            horizontal line
    # Now draw rows from 8 down to 1
    for i in range (7, -1, -1):
        # Draw each row
        linetodraw = ""
        for j in range(8):
            linetodraw += " | " + board[i][j] # for each column we out put a vertical line followed by a letter designating the object we have
        linetodraw += " |" # print one vertical line to end the string
        print(linetodraw)
        print("-----------------------------------")
    print("   a   b   c   d   e   f   g   h")
    global score
    print("Current Score: ", score)


def IsValid(move):
    # returns true if the move is valid, false otherwise
    # check length of move
    if len(move) != 3:
        return False
    if not (move[0] in ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h']):
        return False
    if not (move[1] in ['1', '2', '3', '4', '5', '6', '7', '8']):
        return False
    if not (move[2] in ['u', 'd', 'l', 'r']):
        return False
    # Check that the direction is valid for the given row/column
    # check that first column moves are not left
    if (move[0] =='a') and (move[2] == 'l'):
        return False
    # check that last column moves are not right
    if (move[0] =='h') and (move[2] == 'r'):
        return False
    # Check that bottom row moves are not down
    if (move[1] =='l') and (move[2] == 'd'):
        return False
    # Check that top row moves are not up
    if (move[1] =='8') and (move[2] == 'u'):
        return False
    # No problems, so the move is valid!
    return True

        
# Next we ask the user for a move
def GetMove():
    # get the move from the user
    print("Enter a move by specifying the space and the direction (u,d,l,r). Spaces should list column then row.")
    print("For example, e3u would swap position e3 with the one above, and f7r would swap f7 to the right.")
    
    # Get move
    move = input("Enter move: ")
    
    # loop until we geta good move
    while not IsValid(move):
        move  = input("That's not a valid move! Enter another move")
    return move

def ConvertLetterToCol(col):
    if col == 'a':
        return 0
    elif col == 'b':
        return 1
    elif col == 'c':
        return 2
    elif col == 'd':
        return 3
    elif col == 'e':
        return 4
    elif col == 'f':
        return 5
    elif col == 'g':
        return 6
    elif col == 'h':
        return 7
    else:
        #not a valid column!
        return -1
def SwapPieces(board, move):
    # Swap pieces on board according to move
    # get original position
    origrow = int(move[1])-1 # convert the second element of the move to an integer and subtract 1 since our indices run 0 to 7
    origcol = ConvertLetterToCol(move[0]) 
    
    # Get adjacent position
    if move[2] == 'u':
        newrow = origrow + 1
        newcol = origcol
    elif move[2] == 'd':
        newrow = origrow - 1
        newcol = origcol
    elif move[2] =='l':
        newrow = origrow
        newcol = origcol - 1
    elif move[2] == 'r':
        newrow = origrow
        newcol = origcol + 1
    
    # Swap objects into position
    temp = board[origrow][origcol] # copy one into a temporary position
    board[origrow][origcol] = board[newrow][newcol] #move the other object into the original position
    board[newrow][newcol] = temp
    print("Swapping Pieces")
    
    # Get adjacent position
    # Swap objects in two positions

def RemovePieces(board):
    # remove 3-in-a-row and 3-in-a-coulumn pieces
    # Create board to remove-or-not
    remove = [[0, 0, 0, 0, 0, 0, 0, 0],
              [0, 0, 0, 0, 0, 0, 0, 0],
              [0, 0, 0, 0, 0, 0, 0, 0],
              [0, 0, 0, 0, 0, 0, 0, 0],
              [0, 0, 0, 0, 0, 0, 0, 0],
              [0, 0, 0, 0, 0, 0, 0, 0],
              [0, 0, 0, 0, 0, 0, 0, 0],
              [0, 0, 0, 0, 0, 0, 0, 0]]
    # Go through rows
    for i in range(8):
        for j in range(6):# the first 6 columns
            if (board[i][j] == board[i][j+1]) and (board[i][j] == board[i][j+2]):
                # three in a row are the same
                remove[i][j] = 1;
                remove[i][j+1] = 1;
                remove[i][j+2] =1;
    # go through columns
    for j in range(8):
        for i in range(6):
            if (board[i][j] == board[i+1][j]) and (board[i][j] == board[i+2][j]):
                # three in a row are the same
                remove[i][j] = 1;
                remove[i+1][j] = 1;
                remove[i+2][j] =1;
    # Eliminate those marked
    global score
    removed_any = False
    for i in range (8):
        for j in range(8):
            if remove[i][j] == 1:
                board[i][j] = 0
                score += 1
                removed_any = True
    return removed_any
    print("Removing pieces")


######### DROPPING PIECES #########
## Go through column from bottom to top
## Make list of non-zero elements
## copy those elements in from bottom to top
## Put 0s at top
def DropPieces(board):
    # Drop pieces to fill in blanks
    for j in range(8):
        # make list of pieces in the column
        listofpieces = []
        for i in range(8):
            if board[i][j] != 0:
                listofpieces.append(board[i][j])
    # Copy that list into column
    for i in range(len(listofpieces)):
        board[i][j] = listofpieces[i]
    # fill in remainder of column with 0s
    for i in range(len(listofpieces), 8):
        board[i][j] = 0
    #Drop pieces to fill in blanks
    print("Dropping Pieces")

############ FILLING BLANKS ###############
# Loop through all elements
# If zero, generate a random new piece for that position    
def FillBlanks(board):
    # Fill blanks with random pieces
    for i in range(8):
        for j in range(8):
            if (board[i][j] == 0):
                board[i][j] = choice(['Q', 'R', 'S','T', 'U'])
    print("Filling blanks")

def Update(board, move):
    # Update the board according to move
    SwapPieces(board, move)
    pieces_eliminated = True
    while pieces_eliminated:
        pieces_eliminated = RemovePieces(board)
        DropPieces(board)
        FillBlanks(board)

def doRound(board):  # DONE
    #Perform one round of the game
    #Display current board
    DrawBoard(board)
    #Get move
    move = GetMove()
    #Update board
    Update(board, move)
    # Update turn number
    global turn
    turn += 1

# State main variables
score = 0
turn = 0
goalscore = 100

board = [[0, 0, 0, 0, 0, 0, 0, 0],
         [0, 0, 0, 0, 0, 0, 0, 0],
         [0, 0, 0, 0, 0, 0, 0, 0],
         [0, 0, 0, 0, 0, 0, 0, 0],
         [0, 0, 0, 0, 0, 0, 0, 0],
         [0, 0, 0, 0, 0, 0, 0, 0],
         [0, 0, 0, 0, 0, 0, 0, 0],
         [0, 0, 0, 0, 0, 0, 0, 0]]

## Initialize game
Initialize(board)

# while game is not over

while ContinueGame(score, goalscore):
    # Do a round of the game
    doRound(board)  
    
    
