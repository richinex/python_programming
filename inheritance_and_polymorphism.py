# -*- coding: utf-8 -*-
"""
Created on Mon Jan 27 20:22:20 2020

@author: Richard Chukwu
"""

### OBJECTS WITH INHERITANCE AND POLYMORPHISM ##############
## Children inheriting the fundamental traits from their parents
# Remember an object lets us group data and functions together into one package. we could put a lot of data into one package. Objects are defined within the class.

## Classes:
# Defined by a class name
# Set of variables ("attributes")
# Set of functions("methods")

### For example recording information about gift ###########
class Gift:
    def __init__(self, giver = "", recipient = "", gift = "", occasion = "", date = "", value = 0): #init constructor that gets called for every object
        self._giver = giver
        self._recipient = recipient
        self._gift = gift
        self._occasion = occasion
        self._date = date
        self._value = value
        
    def setGiver(self, giver): # accessor functions
        self._giver = giver
        
    def getGiver(self):
        return self._giver
    
    def setRecipient(self, recipient):
        self._recipient = recipient
        
    def getRecipient(self, recipient):
        return self._recipient
        
# To create an object, we assign a variable a new instance of that class. We can then call our methods on that object or access the attributes of that object
my_birthday_gift = Gift("Mom", "me", "shirt", "birthday","2015", 25.00)
my_birthday_gift.setGiver("Sister")
print(my_birthday_gift.getRecipient())

## The above is an example of encapsulation(the fundamental idea of OOP), i.e putting together the data, and functions that work on that data, into a single package

###Imagine we have a program which we want to keep track of the statistics for different players on a sports team. Players with diff positions will have diff statistics that are relevant to them. The goalie will have goals, strikers will have strikesm a baseball pitcher will have different statistics.

### Using Baseball ####
## Ouarterback
    # Name
    # Team
    # passes attempted
    # Completions
    # passing yards
    
## Statistics to be computed:
    # Percentage of completed passes
    # Average yard gained per pass
    
## ## Running back
    # Name
    # Team
    # Rushes
    # Rushing yards

## We could continue in this way with other positions
## Since name and team are shared attributes, we create a base type
    
## Footbal Player with commonm attributes (name, team) accross all positions
    
### Football Player (parent)
    #Name
    #Team
    
### Quarterback (a Football Player plus)(child inherits name and team)
    #Passes Attempted
    #Completions
    #Passing Yards
    
### Running Back (a Football Player plus)(child inherits name and team)
    # Rushes
    # Rushing Yards
    
class FootballPlayer:
    name = "John Doe"
    team = "None"
    
class QuarterBack(FootballPlayer):
    pass_attempts = 0
    completions = 0
    pass_yards = 0

player1 = QuarterBack()
print(player1.name)
print(player1.pass_yards)
    
class RunningBack(FootballPlayer):
    rushes = 0
    rush_yards = 0
    
player1 = QuarterBack()
player1.name = "John"
player1.team = "cowboys"
player1.pass_attempts = 10
player1.completions = 6
player1.pass_yards = 57

player2 = RunningBack()
player2.name = "Joe"
player2.team = "Eagles"
player2.rushes = 12
player2.rush_yards = 73

### We can also inherit methods ######
class FootballPlayer: # parent or base or super class
    name = "John Doe"
    team = "None"
    years_in_league = 0
    def printPlayer(self):
        print(self.name+" playing for the "+self.team+":")
        
        
    
class QuarterBack(FootballPlayer): #Child or derived or sub class
    pass_attempts = 0
    completions = 0
    pass_yards = 0
    def completionRate(self):
        return self.completions/self.pass_attempts
    def yardsPerAttempt(self):
        return self.pass_yards/self.pass_attempts
        


    
class RunningBack(FootballPlayer):
    rushes = 0
    rush_yards = 0
    def yardsPerRush(self):
        return self.rush_yards/self.rushes
    
player1 = QuarterBack()
player1.name = "John"
player1.team = "cowboys"
player1.pass_attempts = 10
player1.completions = 6
player1.pass_yards = 57

player2 = RunningBack()
player2.name = "Joe"
player2.team = "Eagles"
player2.rushes = 12
player2.rush_yards = 73

player1.printPlayer()
print("Completion rate:", player1.completionRate(), "for", player1.yardsPerAttempt(), "yards per attempt")
player2.printPlayer()
print(player2.yardsPerRush(), "yards per rush")

## Classes can inherit properties from multiple parents. Also called multiple inheritance. but please stay away from it. it is also very rare that multiple inheritance is a solution to your problem

### POLYMORPHISM #######
# Polymorphism means a function, or method can take on many different forma depending on the context
# let's say we want to access whether each player is good or not according to some measure we devise. However each position metrics is different so what do we do. We define anopther method
class FootballPlayer: # parent or base or super class
    name = "John Doe"
    team = "None"
    years_in_league = 0
    def printPlayer(self):
        print(self.name+" playing for the "+self.team+":")
    def isGood(self):
        print("Error! isGood is not defined!")
        return False
    
class QuarterBack(FootballPlayer): #Child or derived or sub class
    pass_attempts = 0
    completions = 0
    pass_yards = 0
    def completionRate(self):
        return self.completions/self.pass_attempts
    def yardsPerAttempt(self):
        return self.pass_yards/self.pass_attempts
    def isGood(self): # also create the isGood()
        return(self.yardsPerAttempt() > 7)
        
   
class RunningBack(FootballPlayer):
    rushes = 0
    rush_yards = 0
    def yardsPerRush(self):
        return self.rush_yards/self.rushes
    def isGood(self):#also create the isGood()
        return (self.yardsPerRush() > 4)

player1 = QuarterBack()
player1.name = "John"
player1.team = "cowboys"
player1.pass_attempts = 10
player1.completions = 6
player1.pass_yards = 57

player2 = RunningBack()
player2.name = "Joe"
player2.team = "Eagles"
player2.rushes = 12
player2.rush_yards = 73

# We create a list and add player1 and player 2 into it
playerlist = []
playerlist.append(player1)
playerlist.append(player2)

for player in playerlist:
    player.printPlayer()
    if (player.isGood()):
        print("  is a GOOD player")
    else:
        print("  is NOT a GOOD player")
        
## What happens is that when the python compiler sees the call to isGood, it first looks for the isGood method in the child class, if that method doesnt exist in the chile class then it looks at the parent. This is an example of polymorphism. The isGood method is polymorphic
        
## Another way ###
# we could also define a printGood method in the way below

class FootballPlayer: # parent or base or super class
    name = "John Doe"
    team = "None"
    years_in_league = 0
    def printPlayer(self):
        print(self.name+" playing for the "+self.team+":")
    def printGood(self): # checks if the isGood method returns True of False
        if (self.isGood()):
            print("  is a Good player")
        else:
            print("  is NOT a good player")
    
class QuarterBack(FootballPlayer): #Child or derived or sub class
    pass_attempts = 0
    completions = 0
    pass_yards = 0
    def completionRate(self):
        return self.completions/self.pass_attempts
    def yardsPerAttempt(self):
        return self.pass_yards/self.pass_attempts
    def isGood(self): # also create the isGood()
        return(self.yardsPerAttempt() > 7)
        
   
class RunningBack(FootballPlayer):
    rushes = 0
    rush_yards = 0
    def yardsPerRush(self):
        return self.rush_yards/self.rushes
    def isGood(self):#also create the isGood()
        return (self.yardsPerRush() > 4)

player1 = QuarterBack()
player1.name = "John"
player1.team = "cowboys"
player1.pass_attempts = 10
player1.completions = 6
player1.pass_yards = 57

player2 = RunningBack()
player2.name = "Joe"
player2.team = "Eagles"
player2.rushes = 12
player2.rush_yards = 73

# We create a list and add player1 and player 2 into it
playerlist = []
playerlist.append(player1)
playerlist.append(player2)

for player in playerlist:
    player.printPlayer()
    player.printGood()
    
## A virtual function or an abstract interface is a function that is not really there. We can also inherit from other classes as well.

## Inheritance in exceptions  ###    

##Exceptions are used to catch errors that would otherwise cause a program to crash
## For example

def computeDivision(first, second):
    try:
        division = first/second  
    except TypeError:
        print("You didnt enter two floating point numbers!")
    except ZeroDivisionError:
        print("Don't ive a zero for the second number!")
    except:
        print("There was some other exception")
    else:
        return division
    
## What if we dont have an exception that captures the nature of our problem and we choose to define one. Now we will make our own exception to demonstrate inheritance. First we create a new class with the name of our error.
class MissingChildMethodError(Exception):#inherits all the functionality of the exception class
    pass # we dont have anything special to do with this exception so we define the body as pass meaning it is identical to the identical exception

class FootballPlayer: # parent or base or super class
    name = "John Doe"
    team = "None"
    years_in_league = 0
    def printPlayer(self):
        print(self.name+" playing for the "+self.team+":")
    def isGood(self):
        raise MissingChildMethodError("Error! isGood is not defined!")
    
player1 = FootballPlayer()
player1.name = "John"
player1.team = "cowboys"

try:
    if player1.isGood():
        print(player1.name+" is a GOOD player")
    else:
        print(player1.name+" is NOT a good player.")
    
except MissingChildMethodError:
    print("Whoops - we forgot to define isGood")
    
### Python Modules that make objects much more useable ###
## Included in the standard library ##
    ## They are JSON(javascrip object notation) and pickle ##
    
# JSON is a way of structuring data in a text format
    # Json data is a nice readable string as opposed to binary data
    # Independent of the language it was produced in
    # Most common way data files are transmitted over the web
#var family = [{
#        "name" : "Jason",# attribute name, colon, value
#        "age" : "24",
#        "gender": "male"
#        },
#    {
#     "name" : "kyle",
#     "age" : "21",
#     "gender" : "male"
#     }];
    
## Most python data types can be converted to/from JSON. The Json routines help us converted a piece of data into Json strings.
import json

myfile = open("Filename", "w")
json_string = json.dumps(data)# data we want to convert to json. this gives us a string we can output
myfile.write(json_string+'\n')# new line character helps us separate it from other strings

myfile.close

## To write in the file
import json

myfile = open("Filename", "w")
json_string = json.dumps(data)# data we want to convert to json. this gives us a string we can output
## To read
data = json.loads(json_string) # for basic data types

myfile.close

##For example
## To write
import json

length = 20.0
width = 15
outfile = open("datafile1.txt", "w")
json_string = json.dumps(length)
outfile.write(json_string+'\n')
json_string = json.dumps(width)
outfile.write(json_string+'\n')
json_string = json.dumps("Data for an example")
outfile.write(json_string+'\n')

outfile.close()

## To read the example
import json

infile = open("datafile1.txt", "r")
json_string = infile.readline()
l = json.loads(json_string)
json_string = infile.readline()
w = json.loads(json_string)
json_string = infile.readline()
description = json.loads(json_string)

infile.close

print(description)
print(l*w)

## To seee the power of JSON, we will use it to store list and objects
# The process for writing is just the same. You are writing one large json string

## Using our football example
player1 = QuarterBack()
player1.name = "John"
player1.team = "cowboys"
player1.pass_attempts = 10
player1.completions = 6
player1.pass_yards = 57

player2 = RunningBack()
player2.name = "Joe"
player2.team = "Eagles"
player2.rushes = 12
player2.rush_yards = 73

# We create a list and add player1 and player 2 into it
playerlist = []
playerlist.append(player1)
playerlist.append(player2)

outfile = open("datafile2.dat", "w")

for player in playerlist:
    json_string = json.dumps(player.__dict__)+"\n" # dict will give a dictionary version of the elements of the object
    outfile.write(json_string)

outfile.close()

## The other module that makes it easier to work with more complecx data and read objects in addition to writing them is Pickle

## pickle module lets us read and write data other than strings more easily
## Reads and writes data in binary format
## Works for even more data formats than json
## its in python specific
## Not for sending to other people
## picle produced files can contain malicious data

import pickle
outfile = open("Filename", "wb") #b indicates binary
pickle.dump(data, outfile)

outfile.close()

## An example, say we have
import pickle
account = 134218954
owner = "John Smith"
balance = 1783.45

## Put into BankAccount.dat

outfile = open("BankAccount.dat", "wb")
pickle.dump(account, outfile)
pickle.dump(owner, outfile)
pickle.dump(balance, outfile)

outfile.close()

## To read in:
import pickle

## Read in from BankAccount.dat

infile = open("BankAccount.dat", "rb")
account = pickle.load(infile)
owner = pickle.load(infile)
balance = pickle.load(infile)

infile.close()

## For our player object
import pickle

class FootballPlayer: # parent or base or super class
    name = "John Doe"
    team = "None"
    years_in_league = 0
    def printPlayer(self):
        print(self.name+" playing for the "+self.team+":")
    def printGood(self): # checks if the isGood method returns True of False
        if (self.isGood()):
            print("  is a Good player")
        else:
            print("  is NOT a good player")
    
class QuarterBack(FootballPlayer): #Child or derived or sub class
    pass_attempts = 0
    completions = 0
    pass_yards = 0
    def completionRate(self):
        return self.completions/self.pass_attempts
    def yardsPerAttempt(self):
        return self.pass_yards/self.pass_attempts
    def isGood(self): # also create the isGood()
        return(self.yardsPerAttempt() > 7)
        
   
class RunningBack(FootballPlayer):
    rushes = 0
    rush_yards = 0
    def yardsPerRush(self):
        return self.rush_yards/self.rushes
    def isGood(self):#also create the isGood()
        return (self.yardsPerRush() > 4)

player1 = QuarterBack()
player1.name = "John"
player1.team = "cowboys"
player1.pass_attempts = 10
player1.completions = 6
player1.pass_yards = 57

player2 = RunningBack()
player2.name = "Joe"
player2.team = "Eagles"
player2.rushes = 12
player2.rush_yards = 73

# We create a list and add player1 and player 2 into it

outfile = open("datafilefor.dat", "wb")
playerlist = []
playerlist.append(player1)
playerlist.append(player2)
for player in playerlist:
    pickle.dump(player, outfile)
  
outfile.close()


## To read in our file
infile = open("datafilefor.dat", "rb")
newplayer1 = pickle.load(infile)
newplayer2 = pickle.load(infile)
infile.close()

newplayer1. printPlayer()
if newplayer1.isGood():
    print(" is a Good player.")
else:
    print(" is NOT a good player.")
newplayer2. printPlayer()
if newplayer2.isGood():
    print(" is a Good player.")
else:
    print(" is NOT a good player.")
    
## It's great to use pickle if you have a program that you need to be able to save and later load.
# polymorphism is always used for inheritance
# tkinter relies heavily on OOP
    
## KEYS TO OOP #####
    ## Figure out what the units of description are
    ## Turn those units into classes. eig checking aacount or vehicles
    ## Think about conceptual ideas that you can package together nicely in one class
    ## decide what operations need to be done to the classes.